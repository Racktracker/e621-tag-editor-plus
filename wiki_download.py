#!/usr/bin/env python3
import requests
import time
import json
import os
import zlib


WIKI_LOCATION = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), "data", "wiki.json")
WIKI_COMPRESSED_LOCATION = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), "data", "wiki_compressed.json.bin")
WIKI_VERSION_LOCATION = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), "data", "wiki_version.json")

page = 1
wiki_pages = {}

session = requests.Session()
session.headers.update({"User-Agent": "WikiDownloadScript (by RackTracker)"})

version_time = int(time.time())

while True:
    time.sleep(1)
    result = session.get(
        "https://e621.net/wiki/index.json?order=title&limit=100"
        "&page=%d" % page).json()

    if len(result) == 0:
        break

    for entry in result:
        wiki_pages[entry["title"]] = entry["body"]

    page += 1

    print("Page %d, %d wiki pages so far" % (page, len(wiki_pages)))

result = {
    "pages": wiki_pages,
    "version": version_time
}

with open(WIKI_LOCATION, "w") as f:
    f.write(json.dumps(result))

with open(WIKI_VERSION_LOCATION, "w") as f:
    f.write(json.dumps({"version": version_time}))

with open(WIKI_LOCATION, "rb") as f:
    content = f.read()
    compressed = zlib.compress(content)

    with open(WIKI_COMPRESSED_LOCATION, "wb") as cf:
        cf.write(compressed)

print("Done")
