#!/bin/bash
mkdir -p site/script;
cat src/greasemonkey_header.js src/main.js src/data.js src/ui.js src/settings.js src/suggestions.js src/tag_questions.js src/load.js > site/script/e621_tag_editor.user.js;

mkdir -p site/script/data;

cp data/wiki_compressed.json.bin site/script/data/wiki_compressed.json.bin;
cp data/tags_compressed.json.bin site/script/data/tags_compressed.json.bin;
cp data/wiki_version.json site/script/data/wiki_version.json;
cp data/tags_version.json site/script/data/tags_version.json;
cp data/tag_editor.html site/script/data/tag_editor.html;
cp css/tagger.css site/script/data/tagger.css;
cp src/search_worker.js site/script/data/search_worker.js;
