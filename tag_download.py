#!/usr/bin/env python3
import requests
import time
import json
import os
import zlib


TAG_LIST_LOCATION = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), "data", "tags.json")

TAG_LIST_COMPRESSED_LOCATION = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), "data", "tags_compressed.json.bin")

TAG_LIST_VERSION_LOCATION = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), "data", "tags_version.json")

GENERAL = 0
ARTIST = 1
COPYRIGHT = 3
CHARACTER = 4
SPECIES = 5

CATEGORY_NAMES = {
    GENERAL: "general",
    ARTIST: "artist",
    COPYRIGHT: "copyright",
    CHARACTER: "character",
    SPECIES: "species"
}

CATEGORY_ORDER = [
    ARTIST, CHARACTER, COPYRIGHT, SPECIES, GENERAL
]

page = 1
tag_types = {0: [], 1: [], 2: [], 3: [], 4: [], 5: [], 6: []}
all_tags = []

session = requests.Session()
session.headers.update({"User-Agent": "TagDownloadScript (by RackTracker)"})

version_time = int(time.time())

while True:
    time.sleep(1)
    tags = session.get(
        "https://e621.net/tag/index.json?order=count&limit=500"
        "&page=%d" % page).json()

    minimum_count_reached = False

    for tag in tags:
        if tag["type"] in [GENERAL] and tag["count"] >= 40:
            add_tag = True
        elif tag["type"] in [ARTIST, COPYRIGHT, CHARACTER, SPECIES] \
                and tag["count"] >= 4:
            add_tag = True
        else:
            add_tag = False

        if add_tag:
            all_tags.append([
                tag["name"],
                tag["count"],
                tag["type"]
            ])

        minimum_count_reached = True if tag["count"] < 4 else False

    if minimum_count_reached:
        break

    page += 1

    print("Page %d, %d tags so far" % (page, len(all_tags)))

categories = [
    (CATEGORY_NAMES[category_id], category_id, i)
    for i, category_id in enumerate(CATEGORY_ORDER)
]

result = {
    "tags": all_tags,
    "categories": categories,
    "version": version_time
}

with open(TAG_LIST_LOCATION, "w") as f:
    f.write(json.dumps(result))

with open(TAG_LIST_VERSION_LOCATION, "w") as f:
    f.write(json.dumps({"version": version_time}))

with open(TAG_LIST_LOCATION, "rb") as f:
    content = f.read()
    compressed = zlib.compress(content)

    with open(TAG_LIST_COMPRESSED_LOCATION, "wb") as cf:
        cf.write(compressed)

print("Done")
