Tagger.prototype.toggleEditor = function() {
    if (this.open) {
        this.closeEditor();
    } else {
        this.openEditor();
    }
};

Tagger.prototype.openEditor = function() {
    if (!this.launched) {
        // Only load these when opening the tag editor, since they can
        // take noticeable amounts of RAM
        this.launched = true;
        this.loadHelperData();
    }

    $("#tag_editor").show();
    this.visible = true;

    // Scroll up
    $("html, body").animate({
        scrollTop: 0
    }, 1000);

    $("#tag_editor_edit").focus();
};

Tagger.prototype.closeEditor = function() {
    $("#tag_editor").hide();
    this.visible = false;
};

Tagger.prototype.resizeEditor = function() {
    // 25px padding on every side
    var editorHeight = $(window).height() - (25 * 2);
    var editorWidth = $(window).width() - (25 * 2);

    $("#tag_editor").height(editorHeight);
    $("#tag_editor").width(editorWidth);

    $("#tag_editor_tag_list").css(
        "max-height", $("#tag_editor").height() * 0.7);
    $(".tag-editor-right-column").css("height", editorHeight + "px");

    // Resize the post to fit the image on screen with 5px padding
    var fitWidth = (editorWidth * 0.5) - (2 * 5);
    var fitHeight = (editorHeight * 0.94) - (2 * 5);
    var origHeight = $("#tag_editor_post > [data-height]").attr("data-height");
    var origWidth = $("#tag_editor_post > [data-width]").attr("data-width");

    var fitAspect = fitWidth / fitHeight;
    var imageAspect = origWidth / origHeight;

    var scaleFactor = 0.0;
    if (fitAspect > imageAspect) {
        scaleFactor = fitHeight / origHeight;
    } else {
        scaleFactor = fitWidth / origWidth;
    }

    var newWidth = scaleFactor * origWidth;
    var newHeight = scaleFactor * origHeight;

    $("#tag_editor_post").css("width", newWidth + "px");
    $("#tag_editor_post").css("height", newHeight + "px");
};

Tagger.prototype.resizeSnippet = function(parentElement) {
    var x = $(parentElement).position()["left"]
            + $(parentElement).width()
            + 50;
    var y = $(parentElement).position()["top"];

    // Check for overflow
    var viewHeight = $(window).height();

    if (y + (viewHeight * 0.35) > viewHeight) {
        y = viewHeight - (viewHeight * 0.35);
    }

    $("#tag_editor_snippet").css("left", x + "px");
    $("#tag_editor_snippet").css("top", y + "px");
};

Tagger.prototype.showMessage = function(message) {
    this.data.editMessage = message;
};

Tagger.prototype.openDialog = function(dialogId) {
    if (this.data.dialogOpen) {
        return;
    };

    var self = this;

    $(`#tag_editor_${dialogId}_dialog`).dialog({
        minWidth: 500,
        maxWidth: 1200
    });
};

Tagger.prototype.openSaveDialog = function() {
    this.data.saveDialogOpen = true;

    var self = this;

    $("#tag_editor_save_dialog").show();
    $("#tag_editor_save_dialog").dialog({
        dialogClass: "no-close",
        minWidth: 500,
        maxWidth: 1200,
        modal: true,
        buttons: {
            "Save changes": function() {
                var dialog = this;

                var cancelBtn = $(".ui-dialog-buttonpane button:contains('Cancel')");
                var confirmBtn = $(".ui-dialog-buttonpane button:contains('Save changes')");

                $(cancelBtn).button("disable");
                $(confirmBtn).button("disable");

                self.saveTags().then(() => {
                    self.data.saveDialogOpen = false;
                    $(cancelBtn).button("enable");
                    $(confirmBtn).button("enable");
                    $(dialog).dialog("close");
                });
            },
            "Cancel": function() {
                self.data.saveDialogOpen = false;
                $(this).dialog("close");
            }
        },
        close: function() {
            self.data.saveDialogOpen = false;
        }
    });
};

Tagger.prototype.showWikiDialog = function(name) {
    if (!this.data.wikiPageNames.has(name)) {
        return;
    }

    if (this.data.openWikiDialogs.filter(d => d.name == name).length == 1) {
        return;
    }

    // For dialogs, get the actual wiki page using a HTTP request
    $.get({
        url: "https://e621.net/wiki/show",
        data: {title: name},
    }).then((content) => {
        var wikiContent = $(content).find("#wiki-body").children()[0];

        // Make each link open a new tab so that the user doesn't
        // accidentally leave the page
        $($(wikiContent).find("a")).each(function() {
            $(this).attr("target", "_blank");
        });

        wikiContent = $(wikiContent).html();

        this.data.openWikiDialogs.push({
            name: name,
            content: wikiContent
        });
    });
};

Tagger.prototype.closeWikiDialog = function(name) {
    this.data.openWikiDialogs = this.data.openWikiDialogs.filter(
        d => d.name != name
    );
};

Tagger.prototype.showSnippet = function(snippet, parentElement) {
    var self = this;

    this.data.currentSnippet = snippet;

    window.setTimeout(function() {
        // FIXME: Hack to reposition the element by delaying the
        // operation by a small moment
        self.resizeSnippet(parentElement);
    }, 10);
};

Tagger.prototype.showWikiSnippet = function(query, reason, parentElement) {
    var self = this;

    if (query.length > 0 && (query[0] == "+" || query[0] == "-")) {
        query = query.slice(1);
    }

    var snippet = {
        name: query,
        type: "wiki",
        reason: reason
    };

    this.data.snippetTimestamp = new Date().getTime();
    var startTimestamp = this.data.snippetTimestamp;

    if (this.data.wikiPageNames.has(query)) {
        this.getWikiPage(query).then((content) => {
            snippet["page"] = content;

            if (self.data.snippetTimestamp == startTimestamp) {
                self.showSnippet(snippet, parentElement);
            }
        });
        if (self.data.localDataInUse) {
            snippet["page"] = self.data.localData.wiki[query];
            this.showSnippet(snippet, parentElement);
        } else {
            this.db.wiki.get({name: query}).then(function(entry) {
                snippet["page"] = entry.content;

                if (self.data.snippetTimestamp == startTimestamp) {
                    self.showSnippet(snippet, parentElement);
                }
            });
        }
    } else {
        this.hideSnippet();
    }
};

Tagger.prototype.showSuggestionSnippet = function(suggestion, parentElement) {
    var self = this;

    var snippet = {
        name: suggestion.tag,
        reason: suggestion.reason,
        type: "suggestion"
    }

    this.data.snippetTimestamp = new Date().getTime();
    var startTimestamp = this.data.snippetTimestamp;

    if (this.data.wikiPageNames.has(suggestion.tag)) {
        this.getWikiPage(suggestion.tag).then((content) => {
            snippet["page"] = content;

            if (self.data.snippetTimestamp == startTimestamp) {
                self.showSnippet(snippet, parentElement);
            }
        });
    } else {
        self.showSnippet(snippet, parentElement);
    }
};

Tagger.prototype.hideSnippet = function() {
    this.data.currentSnippet = null;
    this.data.snippetTimestamp = new Date().getTime();
};

Tagger.prototype.inputChanged = function() {
    var query = $("#tag_editor_edit").val();
    query = query.toLowerCase();
    this.data.lastQuery = query;

    this.showWikiSnippet(query, null, $("#tag_editor_edit"));
    $("#tag_editor_edit").val(query);
};

Tagger.prototype.updatePageElements = function() {
    // Update different tag-related elements (not just our tag editor)
    // in this page to have consistent information
    $("#post_tags").val(this.data.post.tags.join(" "));
    $("#post_old_tags").val(this.data.post.siteTags.join(" "));

    // Update the "Tag Editor" link if we have unsaved changes
    $("#tag_editor_open_link").text(
        this.hasUnsavedChanges()
        ? "Tag Editor Plus (unsaved changes)" : "Tag Editor Plus");
};

Tagger.prototype.processInput = function() {
    var query = $("#tag_editor_edit").val();

    if (query.length == 0) {
        return;
    }

    switch(query) {
        case "/s":
        case "/save":
            if (this.hasUnsavedChanges()) {
                this.openSaveDialog();
            }
            return;

        case "/q":
        case "/quit":
            this.closeEditor();
            return;

        case "/as":
        case "/addsuggestions":
            for (var suggestion of this.data.suggestions) {
                this.addSuggestion(suggestion.tag);
            }
            return;
    };

    var action = "add";
    var tag = query;

    switch(query[0]) {
        case "+":
            action = "force_add";
            tag = query.slice(1);
            break;

        case "-":
            action = "remove";
            tag = query.slice(1);
            break;
    }

    switch(action) {
        case "add":
            this.addTagFromInput(tag, false);
            break;

        case "force_add":
            this.addTagFromInput(tag, true);
            break;

        case "remove":
            this.removeTagFromInput(tag);
            break;
    };
};

Tagger.prototype.installUI = function() {
    var self = this;

    if (self.installed || self.installing) {
        return;
    }

    self.installing = true;

    GM.getResourceUrl("tagEditorHTML").then((url) => {
        return $.get(url);
    }).then((data) => {
        self.templates["tag_editor"] = data;

        // Add the HTML content into the page
        $("body").append(self.templates["tag_editor"]);
        $("#tag_editor").hide();

        // Get the viewed post's HTML content so we can display it
        // inside the editor
        self.data.postHtml = $(
            ".content > div[id != 'ad-leaderboard']").first().html();

        // Init the Vue app
        self.vue = new Vue({
            el: "#tag_editor",
            data: self.data,
            watch: {
                settings: {
                    handler: (newSettings, oldSettings) => {
                        self.saveSettings();

                        self.updateChecklist();
                    },
                    deep: true
                }
            },
            computed: {
                postCategoriesAndTags: function() {
                    var entries = [];

                    for (var category of self.data.tagCategories) {
                        var categoryId = category.id;
                        entries.push({
                            tag: false,
                            type: "category",
                            category: category.name,
                            name: category.name
                        });

                        if (!(categoryId in self.data.post.tagsByCategory)) {
                            continue;
                        }

                        for (var tag of self.data.post.tagsByCategory[categoryId]) {
                            entries.push({
                                tag: true,
                                type: "tag",
                                category: category.name,
                                nameSegments: self.separateMatches(tag, self.data.lastQuery),
                                name: tag
                            });
                        }
                    }

                    if (self.data.metaTags.length > 0) {
                        entries.push({
                            tag: false,
                            type: "category",
                            category: "Meta",
                            name: "Meta"
                        });

                        for (var metaTag of self.data.metaTags) {
                            entries.push({
                                tag: true,
                                type: "metaTag",
                                category: "Meta",
                                nameSegments: self.separateMatches(metaTag, self.data.lastQuery),
                                name: metaTag
                            });
                        }
                    }

                    return entries;
                },
                addedTags: function() {
                    return self.getAddedTags();
                },
                removedTags: function() {
                    return self.getRemovedTags();
                },
                editorReady: function() {
                    return self.isEditorReady();
                }
            },
            methods: {
                addTag: function(tag) { self.addTag(tag); },
                removeTag: function(tag) { self.removeTag(tag); },
                addMetaTag: function(tag) { self.addMetaTag(tag); },
                removeMetaTag: function(tag) { self.removeMetaTag(tag); },
                addTagFromInput: function(tag) { self.addTagFromInput(tag); },
                removeTagFromInput: function(tag) {
                    self.removeTagFromInput(tag);
                },
                hasTag: function(tag) { return self.hasTag(tag); },
                addSuggestion: function(tag) { self.addSuggestion(tag); },
                removeSuggestion: function(tag) { self.removeSuggestion(tag); },
                openDialog: function(templateId) {
                    self.openDialog(templateId);
                },
                openSaveDialog: function() {
                    self.openSaveDialog();
                },
                closeEditor: function() { self.closeEditor(); },
                showWikiSnippet: function(tag, reason, event) {
                    self.showWikiSnippet(tag, reason, event.target);
                },
                showWikiDialog: function(name) {
                    self.showWikiDialog(name);
                },
                showSuggestionSnippet: function(suggestion, event) {
                    self.showSuggestionSnippet(suggestion, event.target);
                },
                hideSnippet: function() { self.hideSnippet(); },
                resizeSnippet: function() { self.resizeSnippet(); },
                completeQuestion: function(question) {
                    self.completeQuestion(question);
                },
                restoreCompletedQuestion: function(question) {
                    self.restoreCompletedQuestion(question);
                },
                toggleQuestion: function(question) {
                    self.toggleQuestion(question);
                }
            }
        });

        self.installed = true;

        // Resize the editor to fit on screen
        self.resizeEditor();

        $(window).resize(function() {
            self.resizeEditor();
        });

        // Attach autocomplete to input
        $("#tag_editor_edit").autocomplete({
            source: function(request, resp) {
                self.fuzzyFindTags(
                    request["term"],
                    function(matches) {
                        matches = matches.map(
                            match => ({
                                "label": match["match"]
                                         + ` (${match['count']})`
                                         + (match["wiki"] ? " (w)" : ""),
                                "value": match["match"]
                            })
                        );

                        resp(matches);
                    }
                );
            },
            search: function(e, ui) {
                // Performance will eat shit if we don't do this
                // https://stackoverflow.com/a/43393889/8981236
                $(this).data("ui-autocomplete").menu.bindings = $();
            },
            focus: function(e, ui) {
                self.showWikiSnippet(
                    ui["item"]["value"],
                    null,
                    $("#tag_editor_edit")
                );
            },
            maxShowItems: 8,
            delay: 200,
            classes: {
                "ui-autocomplete": "tag-editor-autocomplete-item",
                "ui-menu-item": "tag-editor-autocomplete-item"
            }
        });

        // React to when the tag edit input is pressed
        $("#tag_editor_edit").keypress(function(e) {
            if (e.which == 13) { // Enter key
                self.processInput();
                $("#tag_editor_edit").val("");
                $("#tag_editor_edit").autocomplete("close");
                return false;
            }
        });

        // React to query changing
        $("#tag_editor_edit").bind("change paste keyup", function() {
            self.inputChanged();
        });
    });
};

Vue.component("wiki-dialog", {
    props: ["wikiPage"],
    mounted: function() {
        var elementId = "#" + CSS.escape(`tag_editor_wiki_${this.wikiPage.name}_dialog`);
        $(elementId).show().dialog({
            minWidth: 300,
            minHeight: 300,
            width: $(window).width() * 0.4,
            height: $(window).height() * 0.3,
            maxHeight: $(window).height() * 0.9,
            maxWidth: $(window).width() * 0.9,
            close: () => { tagger.closeWikiDialog(this.wikiPage.name); }
        });
    },
    template: (
        `<div style="display: none;" :id="'tag_editor_wiki_' + wikiPage.name + '_dialog'" class="tag-editor-dialog" :title="wikiPage.name">
             <h5>{{ wikiPage.name }}
                <small>(<a target="_blank" :href="'https://e621.net/wiki/show/' + wikiPage.name">wiki</a>,
                        <a target="_blank" :href="'https://e621.net/post/index/1/' + wikiPage.name">posts</a>)</small>
             </h5>
             <div v-html="wikiPage.content"></div>
         </div>`
    )
});
