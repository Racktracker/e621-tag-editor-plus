var tagger = new Tagger();
window.tagger = tagger;

if (!window.taggerInstalled) {
    window.taggerInstalled = true;
    if (tagger.isUserLoggedIn() && tagger.isPostPage()) {
        // We're logged in and in a post page; install the tag editor
        console.log("Installing e621 tag editor");
        tagger.install();
    }
};
