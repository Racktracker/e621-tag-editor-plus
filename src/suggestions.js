var ResolutionQuestions = function() {
    var tags = [];

    var dataWidth = Number($("meta[property='og:image:width']").attr("content"));
    var dataHeight = Number($("meta[property='og:image:height']").attr("content"));

    if (dataWidth >= 1600 || dataHeight >= 1200) {
        tags.push(createTag(
            "hi_res",
            {reason: `Post resolution is ${dataWidth}x${dataHeight} ` +
                     `<b>and</b> width ≥ 1600px <b>or</b> height ≥ 1200px`}
        ));
    }

    if (dataWidth >= 3200 || dataHeight >= 2400) {
        tags.push(createTag(
            "absurd_res",
            {reason: `Post resolution is ${dataWidth}x${dataHeight} ` +
                     `<b>and</b> and width ≥ 3200px <b>or</b> height ≥ 2400px`}
        ));
    }

    if (dataWidth >= 10000 || dataHeight >= 10000) {
        tags.push(createTag(
            "superabsurd_res",
            {reason: `Post resolution is ${dataWidth}x${dataHeight} ` +
                     `<b>and</b> width ≥ 10000px <b>or</b> height ≥ 10000px`}
        ));
    }

    if (dataWidth <= 500 && dataHeight <= 500) {
        tags.push(createTag(
            "low_res",
            {reason: `Post resolution is ${dataWidth}x${dataHeight} ` +
                     `<b>and both</b> width ≤ 500px <b>and</b> height ≤ 500px`}
        ));
    }

    return tags;
};

var AspectRatioQuestions = function() {
    var tags = [];

    var dataWidth = Number($("meta[property='og:image:width']").attr("content"));
    var dataHeight = Number($("meta[property='og:image:height']").attr("content"));

    var ratio = (dataWidth / dataHeight).toFixed(3);

    if (ratio === "1.778") { // 16:9
        tags.push(createTag(
            "16:9",
            {reason: "Post has the 16:9 aspect ratio"}
        ));
    } else if (ratio === "1.600") { // 16:10
        tags.push(createTag(
            "16:10",
            {reason: "Post has the 16:10 aspect ratio"}
        ));
    } else if (ratio === "1.500") { // 3:2
        tags.push(createTag(
            "3:2",
            {reason: "Post has the 3:2 aspect ratio"}
        ));
    } else if (ratio === "1.333") { // 4:3
        tags.push(createTag(
            "4:3",
            {reason: "Post has the 4:3 aspect ratio"}
        ));
    } else if (ratio === "1.250") { // 5:4
        tags.push(createTag(
            "5:4",
            {reason: "Post has the 5:4 aspect ratio"}
        ));
    }

    return tags;
};

var AnimationQuestions = function() {
    var tags = [];

    var isVideo = $("meta[property='og:type']").attr("content").startsWith("video.");
    var isGIF = false;

    if ($("#image").length == 1) {
        var imageSource = $("#image").attr("src");

        if (imageSource.endsWith(".gif")) {
            isGIF = true;
        }
    }

    if (isVideo) {
        tags.push(createTag(
            "animated",
            {reason: "Post appears to be a video"}
        ));
    } else if (isGIF) {
        tags.push(createTag(
            "animated",
            {reason: "Post <b>could be</b> a video"}
        ));
    }

    return tags;
};

var CollaborationQuestions = function() {
    var tags = [];

    if (tagger.data.post.tagsByCategory[1].length > 1) {
        tags.push(createTag(
            "collaboration",
            {reason: "Post has more than one artist tag"}
        ));
    }

    return tags;
};

// Tag helpers
var Q_BASE = 0;
var Q_TAG = 1;
var Q_IF = 2;
var Q_ADD = 3;
var Q_QUESTION = 4;
var Q_IMPLIES = 5;
var Q_META_TAG = 6;
var Q_FUNCTION = 7;

var createSuggestions = function(argsByType, reason) {
    var suggestions = [];
    var impliesFuncs = [];

    for (var tag of argsByType[Q_TAG]) {
        suggestions.push({
            tag: tag,
            name: tag,
            type: "tag",
            reason: reason
        });
    }

    for (var metaTag of argsByType[Q_META_TAG]) {
        suggestions.push(metaTag());
    }

    for (var func of argsByType[Q_FUNCTION]) {
        suggestions.push(...func());
    }

    for (var implFunc of argsByType[Q_IMPLIES]) {
        impliesFuncs.push(implFunc);
    }

    return suggestions;
};

var createTag = function(
    name, { type="tag", reason="" } = {}) {
    return {
        tag: name,
        name: name,
        type: type,
        reason: reason
    };
};

var getSuccessCount = function(argsByType, countToMax=null) {
    var successCount = 0;

    for (var tag of argsByType[Q_TAG]) {
        successCount += tagger.hasTag(tag) ? 1 : 0;
        if (successCount === countToMax) {
            return successCount;
        }
    }

    for (var metaTag of argsByType[Q_META_TAG]) {
        successCount += tagger.hasTag(metaTag.name) ? 1 : 0;
        if (successCount === countToMax) {
            return successCount;
        }
    }

    for (var ifFunc of argsByType[Q_IF]) {
        successCount += ifFunc() ? 1 : 0;
        if (successCount === countToMax) {
            return successCount;
        }
    }

    return successCount;
};


var ifNOrMore = function(argsByType, requiredCount) {
    var successCount = getSuccessCount(argsByType, requiredCount);

    return successCount >= requiredCount;
};

var ifNOrLess = function(argsByType, requiredCount) {
    var successCount = getSuccessCount(argsByType, requiredCount + 1);

    return successCount <= requiredCount;
};

var ifN = function(argsByType, requiredCount) {
    var successCount = getSuccessCount(argsByType, requiredCount + 1);

    return successCount == requiredCount;
};

var getConditionCount = function(argsByType) {
    return argsByType[Q_TAG].length
        + argsByType[Q_META_TAG].length
        + argsByType[Q_IF].length;
};

var getTagCount = function(argsByType) {
    return argsByType[Q_TAG].length + argsByType[Q_META_TAG].length;
};

var getArgsByType = function(args) {
    var result = {
        [Q_TAG]: [],
        [Q_IF]: [],
        [Q_ADD]: [],
        [Q_QUESTION]: [],
        [Q_IMPLIES]: [],
        [Q_META_TAG]: [],
        [Q_FUNCTION]: []
    };

    for (var arg of args) {
        if (typeof arg === "number") {
            continue;
        } else if (typeof arg === "string") {
            result[Q_TAG].push(arg);
        } else if ("type" in arg) {
            result[arg.type].push(arg);
        } else {
            result[Q_FUNCTION].push(arg);
        }
    }

    return result;
};

var toUniqueArray = function(entries) {
    var set = new Set();
    var size = set.size;

    for (var entry of entries) {
        set.add(entry);

        if (set.size === size) {
            console.log(`Found a duplicate entry '${entry}'`);
        }

        size = set.size;
    }

    return [...set];
};

var IfAll = function(...args) {
    var call = function() {
        var argsByType = getArgsByType(call.args);
        var successCount = getConditionCount(argsByType);

        return ifN(argsByType, successCount);
    };

    call.type = Q_IF;
    call.args = toUniqueArray(args);
    return call;
};

var IfAny = function(...args) {
    var call = () => {
        var argsByType = getArgsByType(call.args);

        return ifNOrMore(argsByType, 1);
    };

    call.type = Q_IF;
    call.args = toUniqueArray(args);
    return call;
};

var IfNOrMore = function(...args) {
    var call = () => {
        var previousArgs = call.args.slice(0, call.args.length - 1);
        var requiredCount = call.args[call.args.length - 1];

        var argsByType = getArgsByType(previousArgs);

        return ifNOrMore(argsByType, requiredCount);
    };

    call.type = Q_IF;
    call.args = toUniqueArray(args);
    return call;
};

var IfNone = function(...args) {
    var call = () => {
        var argsByType = getArgsByType(call.args);

        return ifN(argsByType, 0);
    };

    call.type = Q_IF;
    call.args = toUniqueArray(args);
    return call;
};

var AddAny = function(...args) {
    var call = () => {
        var argsByType = getArgsByType(call.args);

        if (ifN(argsByType, 0)) {
            return createSuggestions(argsByType, "");
        } else {
            return [];
        }
    };

    call.type = Q_ADD;
    call.args = toUniqueArray(args);
    return call;
};

var AddAll = function(...args) {
    var call = () => {
        var argsByType = getArgsByType(call.args);

        return createSuggestions(argsByType, "");
    };

    call.type = Q_ADD;
    call.args = toUniqueArray(args);
    return call;
};

var Implies = function(...args) {
    var call = function(add) {
        var argsByType = getArgsByType(call.args);

        if (add) {
            for (var tag of argsByType[Q_TAG]) {
                tagger.addTag(tag, undefined, false);
            }
            for (var metaTag of argsByType[Q_META_TAG]) {
                tagger.addMetaTag(metaTag);
            }
        } else {
            for (var tag of argsByType[Q_TAG]) {
                tagger.removeTag(tag, false);
            }
            for (var metaTag of argsByType[Q_META_TAG]) {
                tagger.removeMetaTag(metaTag);
            }
        }
    };

    call.type = Q_IMPLIES;
    call.args = args;
    return call;
};

var Question = function(question) {
    var call = () => {
        return call.question;
    };

    call.type = Q_QUESTION;
    call.question = question;
    return call;
};

var MetaTag = function(tagName) {
    var call = () => {
        return {
            tag: call,
            type: "metaTag",
            name: call.tagName
        };
    };

    call.type = Q_META_TAG;
    call.tagName = tagName;
    return call;
};

var Q = function(...args) {
    var call = () => {
        var argsByType = getArgsByType(call.args);

        var name = null;

        if (argsByType[Q_QUESTION].length > 1) {
            console.log("Wrong amount of Question declarations found!");
            return null;
        } else if (argsByType[Q_QUESTION].length === 1) {
            name = argsByType[Q_QUESTION][0]();
        }

        if (tagger.data.completedQuestions.includes(name)) {
            return null;
        }

        for (var ifFunc of argsByType[Q_IF]) {
            var result = ifFunc();

            if (!result) {
                return null;
            }
        }

        var tags = [];

        for (var addFunc of argsByType[Q_ADD]) {
            tags = tags.concat(addFunc());
        }

        var tagUseCount = tags.filter(
            t => tagger.hasTag(t.tag)).length;

        if (tagUseCount === tags.length) {
            // If all tags have been added, no need to show this question
            return null;
        }

        tags = tags.sort((a, b) => { return a.name.localeCompare(b.name); });

        if (tags.length > 0 && name !== null) {
            return {
                name: name,
                tags: tags
            };
        } else {
            return null;
        }
    };

    call.type = Q_BASE;
    call.args = args;
    propagateImpliesElements(call);

    return call;
};

var TagQuestions = function() {
    var questions = [];

    for (var tagQuestionFunc of tagger.suggestionModel.tagQuestions) {
        var question = tagQuestionFunc();

        if (question !== null) {
            questions = questions.concat(question);
        }
    }

    return questions;
};

var QUESTION_PROVIDERS = [
    /*ResolutionQuestions,
    AspectRatioQuestions,
    AnimationQuestions,
    CollaborationQuestions,*/
    TagQuestions
];

Tagger.prototype.completeQuestion = function(question) {
    this.data.completedQuestions.push(question);
    this.data.completedQuestions.sort();

    this.updateChecklist();
};

Tagger.prototype.restoreCompletedQuestion = function(question) {
    this.data.completedQuestions = this.data.completedQuestions.filter(
        q => q !== question);

    this.updateChecklist();
};

Tagger.prototype.toggleQuestion = function(question) {
    var open = this.data.openedQuestions.includes(question);

    if (open) {
        this.data.openedQuestions = this.data.openedQuestions.filter(
            q => q != question);
    } else {
        this.data.openedQuestions.push(question);
    }
};

Tagger.prototype.updateChecklist = function() {
    this.data.questions = [];

    for (var provider of tagger.suggestionModel.questionProviders) {
        var questions = provider();

        this.data.questions = this.data.questions.concat(questions);
    }

    this.data.questions.sort((a, b) => {
        if (a.name < b.name) {
            return -1;
        } else if (a.name > b.name) {
            return 1;
        } else {
            return 0;
        }
    });

    // Add tag use count for every question
    for (let question of this.data.questions) {
        question.useCount = this.countAddedTags(question.tags);
    }

    var maximumQuestionCount = this.data.settings.maximumQuestionCount;

    if (maximumQuestionCount < this.data.questions.length) {
        var remainingQuestionCount = this.data.questions.length - maximumQuestionCount;

        this.data.questions = this.data.questions.slice(0, maximumQuestionCount);

        this.data.remainingQuestionCount = remainingQuestionCount;
    } else {
        this.data.remainingQuestionCount = 0;
    }
};

Tagger.prototype.iterImpliedTags = function *(tagName) {
    if (tagName in this.suggestionModel.tagImplications) {
        for (var impliedTag of this.suggestionModel.tagImplications[tagName]) {
            yield* this.iterImpliedTags(impliedTag);
            yield impliedTag;
        }
    }
};

Tagger.prototype.getImpliedTags = function(tag) {
    var tags = [...this.iterImpliedTags(tag)];
    tags = tags.map(t => {
        return {name: t, type: this.getTagType(t)};
    });

    return tags;
};

Tagger.prototype.countAddedTags = function(tags) {
    return tags.filter(t => this.hasTag(t.tag)).length;
};

var propagateImpliesElements = function(elem, implElems=[]) {
    var argsByType = getArgsByType(elem.args);

    if (elem.type === Q_ADD) {
        elem.args.push(...implElems);
    }

    var impliesElements = [...implElems, ...argsByType[Q_IMPLIES]];

    var iterateElements = [...argsByType[Q_IF], ...argsByType[Q_ADD]];

    for (var subElem of iterateElements) {
        propagateImpliesElements(subElem, impliesElements);
    }
};

var iterMetaTags = function *(arg) {
    var argsByType = getArgsByType(arg.args);

    var argsToIterate = [...argsByType[Q_IF], ...argsByType[Q_ADD]];

    for (var func of argsToIterate) {
        yield* iterMetaTags(func);
    }

    for (var metaTag of argsByType[Q_META_TAG]) {
        yield metaTag;
    }
};

var iterImplications = function *(arg) {
    var argsByType = getArgsByType(arg.args);

    for (var ifFunc of argsByType[Q_IF]) {
        yield* iterImplications(ifFunc);
    }

    for (var addFunc of argsByType[Q_ADD]) {
        yield* iterImplications(addFunc);
    }

    var implFuncs = argsByType[Q_IMPLIES].map(
        f => f.args);
    var impliedTags = [];

    for (var implFunc of implFuncs) {
        var implArgsByType = getArgsByType(implFunc);

        // We need only names for meta tags in the implication table
        var metaTagNames = implArgsByType[Q_META_TAG].map(
            metaTag => metaTag()["name"]
        );

        impliedTags.push(
            ...implArgsByType[Q_TAG],
            ...metaTagNames
        );
    }

    if (impliedTags.length == 0) {
        return;
    }

    var tags = [...argsByType[Q_TAG], ...argsByType[Q_META_TAG]];

    for (var tag of tags) {
        yield [tag, impliedTags];
    }
};

// Create tag implications by searching through Question declarations
var createModelData = function(questions) {
    var implications = {};
    var reverseImplications = {};
    var metaTagNames = new Set();

    for (var question of questions) {
        for (var result of iterImplications(question)) {
            var tag = result[0];
            var impliedTags = result[1];

            if (!(tag in implications)) {
                implications[tag] = new Set();
            }

            for (var tagName of impliedTags) {
                if (tagName === tag) {
                    continue;
                }

                implications[tag].add(tagName);
            }

            for (var impliedTag of impliedTags) {
                if (!(impliedTag in reverseImplications)) {
                    reverseImplications[impliedTag] = new Set();
                }

                if (impliedTag === tag) {
                    continue;
                }

                reverseImplications[impliedTag].add(tag);
            }
        }

        for (var metaTag of iterMetaTags(question)) {
            metaTagNames.add(metaTag()["name"]);
        }
    }

    return {
        "implications": implications,
        "reverseImplications": reverseImplications,
        "metaTagNames": metaTagNames
    };
};
