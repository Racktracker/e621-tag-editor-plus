// ==UserScript==
// @name e621 Tag Editor Plus
// @namespace https://racktracker.gitlab.io/
// @description A powerful tagging tool for e621
// @version 0.2.2
// @match https://*.e621.net/post/show/*
// // @require https://cdn.jsdelivr.net/npm/vue@2.6.3/dist/vue.js
// @require https://cdn.jsdelivr.net/npm/vue
// @require https://cdnjs.cloudflare.com/ajax/libs/fuse.js/3.3.0/fuse.min.js
// @require https://code.jquery.com/jquery-3.3.1.min.js
// @require https://code.jquery.com/ui/1.12.1/jquery-ui.min.js
// @require https://unpkg.com/jquery-ui.autocomplete.scroll@0.1.9/jquery.ui.autocomplete.scroll.min.js
// @require https://unpkg.com/jquery.hotkeys@0.1.0/jquery.hotkeys.js
// @require https://unpkg.com/dexie@2.0.4/dist/dexie.js
// @require https://unpkg.com/pako@1.0.6/dist/pako_inflate.min.js
// @resource taggerCSS data/tagger.css
// @resource searchWorkerJS data/search_worker.js
// @resource tagEditorHTML data/tag_editor.html
// @resource tagsCompressedJSON data/tags_compressed.json.bin
// @resource tagsVersionJSON data/tags_version.json
// @resource wikiCompressedJSON data/wiki_compressed.json.bin
// @resource wikiVersionJSON data/wiki_version.json
// @run-at document-end
// @grant GM.getResourceUrl
// ==/UserScript==

var $ = window.jQuery || $;

GM.getResourceUrl("taggerCSS").then((url) => {
    return $.get(url);
}).then((css) => {
    // Polyfill of deprecated GM_addStyle
    // https://github.com/greasemonkey/gm4-polyfill/blob/master/gm4-polyfill.js#L33
    let head = document.getElementsByTagName('head')[0];
    if (head) {
        let style = document.createElement('style');
        style.setAttribute('type', 'text/css');
        style.textContent = css;
        head.appendChild(style);
        return style;
    }
});

// Use a trick to include jQuery CSS
$("head").append(
    '<link '
  + 'href="//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.min.css" '
  + 'rel="stylesheet" type="text/css">'
);

