function Tagger() {
    this.SCRIPT_HOST_URL = "https://racktracker.gitlab.io/e621-tag-editor-plus/script/";

    this.TAG_SNAPSHOT_URL = this.SCRIPT_HOST_URL + "data/tags_compressed.json.bin";
    this.WIKI_SNAPSHOT_URL = this.SCRIPT_HOST_URL + "data/wiki_compressed.json.bin";

    this.UPDATE_FREQUENCY = 3600; // Check for updates once a hour

    this.suggestionModel = {
        questionProviders: QUESTION_PROVIDERS,
        tagQuestions: TAG_QUESTIONS,
        tagImplications: TAG_IMPLICATIONS,
        tagReverseImplications: TAG_REVERSE_IMPLICATIONS,
        metaTagNames: META_TAG_NAMES
    };

    this.templates = {};

    this.installed = false;
    this.installing = false;

    this.launched = false;
    this.visible = false;

    // Fuzzy finder that will be initialized later
    this.fuzzyfinder = null;

    this.db = new Dexie("e621_tagger");
    this.db.version(1).stores({
        wiki: "&name, content",
        tags: "&name, count, categoryId",
        tag_categories: "&id, order, name",
        versions: "&name, version, lastCheck"
    });

    this.finderTags = [];

    // Data used by the templates
    this.data = {
        // Remaining tasks before the tag editor is considered loaded
        remainingTasks: [
            "loadPostData",
            "loadTagSnapshot",
            "loadWikiSnapshot"
        ],

        lastQuery: "",
        lastQueryTime: 0,

        saveDialogOpen: false,

        saveForm: {
            editReason: ""
        },

        currentSnippet: null,
        snippetTimestamp: 0,

        editMessage: null,

        allTags: [],
        allTagsByCategory: {},

        tagCategories: [],

        post: {
            id: null,
            downloadUrl: "#",
            tags: [], // Tags added on the editor
            siteTags: [], // Tags currently saved on e621
            tagsByCategory: {
                0: [],
                1: []
            },
            tagUseCounts: {}
        },

        metaTags: [],

        questions: [],
        remainingQuestionCount: 0,
        openedQuestions: [],
        completedQuestions: [],

        displayCompletedQuestions: false,

        defaultSettings: {
            "maximumQuestionCount": {
                value: 50,
                type: Number
            },
            "displayQuestions": {
                value: true,
                type: Boolean
            },
            "displayTags": {
                value: true,
                type: Boolean
            }
        },
        settings: {},

        suggestions: [],

        wikiPageNames: new Set(),
        openWikiDialogs: [],

        // localData contains wiki and tag data if IndexedDB can't be used
        localDataInUse: false,
        localData: {
            wiki: {}
        },

        // The HTML content for the post being edited. This could contain an
        // image or a video
        postHtml: null
    };
};

Tagger.prototype.isUserLoggedIn = function() {
    return $("#navbar > li:contains('My Account')").length == 1;
};

Tagger.prototype.isPostPage = function() {
    return $("#post-view").length == 1;
};

Tagger.prototype.install = function() {
    var self = this;

    // Load the settings
    this.loadSettings();

    // Add a link to enable the tag editor and make it stand out a bit
    var actionList = $(
        "#post-view > div#right-col > div > h4 > a:contains('Edit')").parent();
    var tagEditorLink = $.parseHTML(
        "<a id='tag_editor_open_link' style='text-shadow: 2px 2px black, -2px -2px black, -2px 0px black, 2px 0px black' href='#'>Tag Editor Plus</a> | ");
    $(tagEditorLink).click(function() {
        self.openEditor();
    });

    $(actionList).prepend(tagEditorLink);

    // Get the post's ID
    var postId = $("meta[property='og:url']").attr("content").split("/").pop();
    postId = Number(postId);

    if (postId !== NaN) {
        this.data.post.id = postId;
        this.data.post.downloadUrl = $(
            ".content a:contains('Download')").attr("href");
        this.installUI();
    }
};

Tagger.prototype.openEditor = function() {
    console.log("Opening editor...");
};

Tagger.prototype.toggleEditor = function() {
    console.log("Toggling editor...");
};
