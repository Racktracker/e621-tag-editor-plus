/* global TAG_QUESTIONS, Question, IfNone, IfAny, IfAll, IfNOrMore, AddAny,
          AddAll, Implies, MetaTag, Q */
var BODY_COLORS = [
    "black_body", "blue_body", "brown_body", "green_body",
    "grey_body", "orange_body", "pink_body", "purple_body",
    "red_body", "white_body", "yellow_body", "cyan_body",
    "tan_body"
];

var TONGUE_COLORS = [
    "pink_tongue", "blue_tongue", "black_tongue", "green_tongue",
    "purple_tongue", "red_tongue", "yellow_tongue", "orange_tongue",
    "grey_tongue", "white_tongue", "teal_tongue", "brown_tongue"
];

var FEATHER_COLORS = [
    "black_feathers", "blue_feathers", "brown_feathers",
    "green_feathers", "grey_feathers", "orange_feathers",
    "pink_feathers", "purple_feathers", "red_feathers",
    "white_feathers", "yellow_feathers", "cyan_feathers",
    "rainbow_feathers", "tan_feathers"
];

var SCALE_COLORS = [
    "black_scales", "blue_scales", "brown_scales", "green_scales",
    "grey_scales", "orange_scales", "pink_scales", "purple_scales",
    "red_scales", "white_scales", "yellow_scales", "cyan_scales",
    "rainbow_scales", "tan_scales"
];

var SKIN_COLORS = [
    "black_skin", "blue_skin", "brown_skin", "green_skin", "grey_skin",
    "orange_skin", "pink_skin", "purple_skin", "red_skin", "white_skin",
    "yellow_skin", "beige_skin", "cyan_skin", "indigo_skin", "tan_skin",
    "teal_skin"
];

var EYE_COLORS = [
    "amber_eyes", "black_eyes", "blue_eyes", "brown_eyes", "green_eyes",
    "grey_eyes", "heterochromia", "orange_eyes", "pink_eyes", "purple_eyes",
    "red_eyes", "rainbow_eyes", "white_eyes", "yellow_eyes"
];

var EAR_COLORS = [
    "black_ears", "brown_ears", "white_ears", "grey_ears", "blue_ears",
    "pink_ears", "yellow_ears", "red_ears", "green_ears", "tan_ears",
    "purple_ears", "orange_ears"
];

var SCLERA_COLORS = [
    "black_sclera", "blue_sclera", "brown_sclera", "cyan_sclera",
    "green_sclera", "grey_sclera", "orange_sclera", "pink_sclera",
    "purple_sclera", "red_sclera", "white_sclera", "yellow_sclera"
];

var TAIL_COLORS = [
    "black_tail", "grey_tail", "white_tail", "blue_tail", "brown_tail",
    "green_tail", "orange_tail", "pink_tail", "purple_tail", "red_tail",
    "yellow_tail", "blonde_tail", "cream_tail", "cyan_tail", "tan_tail",
    "rainbow_tail"
];

var HORN_COLORS = [
    "white_horn", "black_horn", "blue_horn", "brown_horn", "grey_horn",
    "purple_horn", "red_horn", "yellow_horn", "orange_horn", "pink_horn",
    "green_horn", "tan_horn"
];

var HAIR_COLORS = [
    "black_hair", "blonde_hair", "blue_hair", "brown_hair", "cream_hair",
    "green_hair", "grey_hair", "orange_hair", "pink_hair", "purple_hair",
    "rainbow_hair", "red_hair", "tan_hair", "white_hair"
];

var WING_COLORS = [
    "black_wings", "blue_wings", "brown_wings", "green_wings", "grey_wings",
    "orange_wings", "pink_wings", "purple_wings", "red_wings", "white_wings",
    "yellow_wings"
];

var ANTHRO_HUMANOID_TAGS = [
    "human", "anthro", "semi-anthro", "humanoid"
];

var MARKING_COLORS = [
    "black_markings", "blue_markings", "brown_markings", "cyan_markings",
    "green_markings", "grey_markings", "orange_markings", "pink_markings",
    "purple_markings", "red_markings", "teal_markings", "white_markings",
    "yellow_markings"
];

var TAIL_TAGS = [
    "fish_tail", "flaming_tail", "fluffy_tail", "glowing_tail",
    "leaf_tail", "head_tail", "prehensile_tail", "snake_tail",
    "tentacle_tail", "multicolored_tail", "dipstick_tail",
    "small_tail", "big_tail", "huge_tail", "hyper_tail",
    "short_tail", "long_tail", "skinny_tail", "thick_tail",
    "curled_tail", "forked_tail", "mace_tail", "spade_tail",
    "braided_tail", "docked_tail", "frizzy_tail", "spiked_tail",
    "tail_button_warmers", "tail_warmer", "tailband",
    "tail_bell", "tail_belt", "tail_cuff", "tail_garter",
    "tail_orb", "tail_ornament", "tail_ribbon", "tail_bow",
    "tail_jewelry", "tail_ring", "tail_piercing",
    "tail_around_leg", "tail_around_waist", "tail_aside",
    "tail_between_legs", "tail_in_mouth", "tail_in_pants",
    "tail_pillow", "tail_scarf", "balancing_on_tail", "coiled_tail",
    "entwined_tails", "hanging_by_tail", "raised_tail", "tailwag",
    "tail_boner", "tail_coil", "tail_growth", "hand_on_tail",
    "holding_tail", "lifted_by_tail", "tail_biting", "tail_bondage",
    "tail_fondling", "tail_grab", "tail_hug", "tail_lick", "tail_pull",
    "tail_sex", "tail_masturbation", "tail_play", "tail_suck", "tail_feathers"
];

var BEAK_COLORS = [
    "black_beak", "yellow_beak", "grey_beak", "orange_beak", "blue_beak",
    "white_beak", "brown_beak", "red_beak", "pink_beak", "tan_beak"
];

var LEG_TAGS = [
    "spread_legs", "leg_grab", "countershade_legs", "leg_tuft", "legs_tied",
    "leg_wrap", "tail_around_leg", "tail_between_legs", "prosthetic_legs",
    "featureless_legs", "legs_up", "crossed_legs", "one_leg_up", "on_one_leg",
    "raised_leg", "legs_behind_head", "on_hind_legs", "legs_together",
    "hand_on_leg", "leg_markings", "cum_on_leg"
];

var CHARACTERS_IN_SCENE = [
    "solo", "solo_focus", "duo", "duo_focus", "group"
];

var TAG_QUESTIONS = [
    // Programmatic questions
    Q(
        Question("Artwork - resolution?"),
        AddAll(ResolutionQuestions)
    ),
    // Questions
    Q(
        Question("Artwork - media type?"),
        AddAll(Implies("traditional_media_(artwork)"),
               "colored_pencil_(artwork)", "crayon_(artwork)",
               "marker_(artwork)", "painting_(artwork)", "pastel_(artwork)",
               "pen_(artwork)", "pencil_(artwork)", "sculpture_(artwork)",
               "watercolor_(artwork)", "gouache_(artwork)"),
        AddAll(Implies("digital_media_(artwork)"),
               "3d_(artwork)", "digital_drawing_(artwork)",
               "digital_painting_(artwork)", "pixel_(artwork)"),
        AddAll("digital_media_(artwork)", "photography_(artwork)",
               "mixed_media")
    ),
    Q(
        Question("Artwork - type?"),
        AddAll("sketch", "unfinished")
    ),
    Q(
        Question("Artwork - pose?"),
        AddAll(Implies("pose"), "pinup", "action_pose"),
        AddAll(Implies("portrait"),
               "full-length_portrait", "three-quarter_portrait",
               "bust_portrait", "half-length_portrait", "headshot_portrait"),
        AddAll("pose", "portrait")
    ),
    Q(
        Question("Artwork - censorship?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll(Implies("censored", "<3"), "<3_censor"),
        AddAll(Implies("convenient_censorship"),
               "hair_covering_breasts"),
        AddAll(Implies("convenient_censorship", MetaTag("tail")),
               "tail_censorship"),
        AddAll(Implies("naturally_censored"), "nipple_tuft"),
        AddAll(Implies("censored"),
               "mosaic_censorship", "ineffective_censorship",
               "creative_censorship", "censor_bar"),
        AddAll("convenient_censorship", "naturally_censored")
    ),
    Q(
        Question("Artwork - overlay?"),
        AddAll(Implies("watermark"),
               "distracting_watermark", "3rd_party_watermark"),
        AddAll("watermark", "signature", "url", "logo")
    ),
    Q(
        Question("Animation - type?"),
        IfAny("animated"),
        Implies("animated"),
        AddAll(Implies("pixel_(artwork)"), "pixel_animation"),
        AddAll("2d_animation", "motion_tweening", "frame_by_frame")
    ),
    Q(
        Question("Animation - length?"),
        IfAny("animated"),
        Implies("animated"),
        AddAll("loop", "long_playtime")
    ),
    Q(
        Question("Year?"),
        AddAny("1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997",
               "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005",
               "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013",
               "2014", "2015", "2016", "2017", "2018", "2019")
    ),
    Q(
        Question("Meta?"),
        AddAll(Implies("story"), "story_in_description", "story_at_source"),
        AddAll(Implies("edit"),
               "cropped", "uncensored", "shopped", "photo_manipulation"),
        AddAll(Implies("edit", "photo_manipulation"), "photomorph"),
        AddAll("story", "lol_comments", "tagging_guidelines_illustrated",
               "meta", "edit")
    ),
    Q(
        Question("Themes?"),
        AddAll("holidays", "video_games")
    ),
    Q(
        Question("Themes - holidays?"),
        IfAny("holidays"),
        Implies("holidays"),
        AddAll("christmas", "new_year", "easter", "halloween", "thanksgiving",
               "valentine's_day")
    ),
    Q(
        Question("Characters - count?"),
        AddAny("solo", "duo", "group", "zero_pictured")
    ),
    Q(
        Question("Characters - in focus?"),
        IfAny("solo_focus", "duo", "group"),
        AddAll("female_focus", "intersex_focus", "male_focus", "solo_focus")
    ),
    Q(
        Question("Characters - gender?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll("male", "female", "intersex", "maleherm", "gynomorph",
               "andromorph", "ambiguous_gender", "girly", "tomboy")
    ),
    Q(
        Question("Characters - body type?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll("anthro", "feral", "humanoid", "taur", "semi-anthro"),
        AddAll(Implies("anthro"), "anthrofied"),
        AddAll(Implies("anthrofied"), "pokémorph", "digimorph"),
        AddAll("ponified"),
        AddAll(Implies("feral"), "feralized")
    ),
    Q(
        Question("Characters - species (real)?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll("human", "amphibian", "antelope", "arthropod",
               "avian", "bovine", "ursine", "canid", "camelid", "caprine",
               "cervid", "dinosaur", "equine", "felid", "gastropod",
               "hybrid", "lagomorph", "mammal", "marine", "marsupial",
               "monotreme", "mustelid", "porcine", "primate", "rodent",
               "scalie", "viverrid", "unknown_species")
    ),
    Q(
        Question("Species - canid?"),
        IfAny("canid"),
        AddAll(Implies("canine", "canis"),
               "wolf", "jackal", "african_golden_wolf", "coyote", "dire_wolf",
               "ethiopian_wolf", "domestic_dog"),
        AddAll(Implies("canine", "canis", "wolf"),
               "arctic_wolf", "red_wolf", "dingo"),
        AddAll(Implies("canine", "canis", "jackal"),
               "black-backed_jackal", "golden_jackal"),
        AddAll(Implies("canine"), "fox")
    ),
    Q(
        Question("Species - domestic dog?"),
        IfAny("domestic_dog"),
        AddAll(Implies("domestic_dog"),
               "husky", "german_shepherd", "doberman", "shih_tzu", "collie",
               "dalmatian", "border_collie", "shiba_inu", "corgi", "poodle",
               "rottweiler", "golden_retriever", "great_dane", "akita",
               "terrier", "malamute", "saluki", "bull_terrier", "labrador",
               "pit_bull", "australian_shepherd", "pomeranian", "basset_hound",
               "cocker_spaniel", "beagle", "siberian_husky", "chihuahua",
               "rough_collie", "bernese_mountain_dog", "samoyed", "sheepdog",
               "saint_bernard", "chinese_crested_dog", "afghan_hound",
               "cavalier_king_charles_spaniel", "chow_chow", "papillon", "pug",
               "dachshund", "irish_setter", "akita_inu", "greyhound",
               "boston_terrier", "borzoi", "jack_russell_terrier", "jindo",
               "whippet", "tamaskan_dog")
    ),
    Q(
        Question("Species - felid?"),
        IfAny("felid"),
        AddAll(Implies("felid"),
               "domestic_cat", "tiger", "lion", "leopard", "cheetah", "lynx",
               "snow_leopard", "cougar", "jaguar", "leopardus", "serval",
               "ocelot", "caracal_(genus)", "caracal", "wildcat",
               "clouded_leopard", "margay", "bobcat", "chinese_mountain_cat",
               "prionailurus", "eurasian_lynx", "leopard_cat",
               "siberian_tiger", "page", "sand_cat", "amur_leopard")
    ),
    Q(
        Question("Species - domestic cat?"),
        IfAny("domestic_cat"),
        AddAll(Implies("domestic_cat"),
               "siamese", "sphynx_(cat)", "maine_coon", "persian_cat",
               "russian_blue", "manx")
    ),
    Q(
        Question("Species - equine?"),
        IfAny("equine"),
        Implies("equine"),
        AddAll(Implies("pony", "horse"), "earth_pony"),
        AddAll(Implies("wings", "unicorn"), "winged_unicorn"),
        AddAll(Implies("horse"),
               "pony", "draft_horse", "american_paint_horse", "appaloosa",
               "friesian"),
        AddAll(Implies("draft_horse"), "clydesdale", "shire"),
        AddAll(Implies("wings"), "pegasus"),
        AddAll(Implies("asinus"), "donkey"),
        AddAll("horse", "unicorn", "zebra", "centaur", "asinus")
    ),
    Q(
        Question("Species - lagomorph?"),
        IfAny("lagomorph"),
        Implies("lagomorph"),
        AddAll(Implies("hare"), "japanese_hare", "arctic_hare"),
        AddAll(Implies("rabbit"), "dutch_rabbit", "rex_rabbit", "lop_rabbit"),
        AddAll("rabbit", "hare")
    ),
    Q(
        Question("Species - rodent?"),
        IfAny("rodent"),
        Implies("rodent"),
        AddAll("mouse", "sciurid", "tree_squirrel", "rat", "ground_squirrel",
               "chipmunk", "beaver", "hamster", "porcupine", "chinchilla",
               "kangaroo_rat", "caviid", "naked_mole-rat", "jerboa",
               "hystricid", "crested_porcupine", "capybara", "dormouse",
               "flying_squirrel", "guinea_pig", "gerbil", "lemming")
    ),
    Q(
        Question("Species - caprine?"),
        IfAny("caprine"),
        Implies("caprine"),
        AddAll(Implies("sheep"), "domestic_sheep", "bighorn_sheep"),
        AddAll(Implies("goat"), "domestic_goat"),
        AddAll("sheep", "goat")
    ),
    Q(
        Question("Species - bovine?"),
        IfAny("bovine"),
        Implies("bovine"),
        AddAll(Implies("cattle"),
               "holstein_friesian_cattle", "hereford_cattle",
               "highland_cattle"),
        AddAll(Implies("bison"), "american_bison"),
        AddAll("cattle", "cape_buffalo",  "bison", "yak", "water_buffalo")
    ),
    Q(
        Question("Species - mustelid?"),
        IfAny("mustelid"),
        Implies("mustelid"),
        AddAll("otter", "ferret", "badger", "weasel", "mink", "wolverine",
               "marten", "honey_badger", "pine_marten", "marbled_polecat")
    ),
    Q(
        Question("Species - fox?"),
        IfAny("fox"),
        Implies("fox"),
        AddAll("fennec", "red_fox", "arctic_fox", "silver_fox", "corsac_fox",
               "marble_fox", "swift_fox")
    ),
    Q(
        Question("Species - cervid?"),
        IfAny("cervid"),
        Implies("cervid"),
        AddAll("reindeer", "moose", "white-tailed_deer", "elk", "mule_deer",
               "tufted_deer")
    ),
    Q(
        Question("Species - avian?"),
        IfAny("avian"),
        Implies("avian"),
        AddAll("gryphon", "bird"),
        AddAll(Implies("bird", "corvid"), "crow", "blue_jay", "raven"),
        AddAll(Implies("bird", "eagle"), "bald_eagle", "golden_eagle"),
        AddAll(Implies("bird", "parrot"),
               "cockatoo", "macaw", "cockatiel", "parakeet"),
        AddAll(Implies("macaw"), "scarlet_macaw"),
        AddAll(Implies("bird", "owl"),
               "snowy_owl", "barn_owl", "great_horned_owl"),
        AddAll(Implies("bird", "vulture"), "bearded_vulture"),
        AddAll(Implies("bird", "falcon"), "peregrine_falcon"),
        AddAll(Implies("bird"),
               "chicken", "corvid", "owl", "duck", "eagle", "penguin",
               "parrot", "harpy", "falcon", "phoenix", "swallow_(bird)",
               "hawk", "vulture", "peafowl", "turkey", "pigeon", "toucan",
               "puffin", "crane", "secretary_bird", "swan", "magpie",
               "cardinal_(bird)", "robin", "flamingo", "woodpecker", "osprey",
               "roadrunner", "dove", "stork", "bluebird", "pheasant", "goose",
               "ostrich", "sparrow", "canary", "heron", "pelican",
               "great_horned_owl", "budgerigar", "albatross")
    ),
    Q(
        Question("Characters - species (fictional)?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll(Implies("pokémon"), "pokémon_(species)"),
        AddAll(Implies("digimon"), "digimon_(species)"),
        AddAll("dragon", "serval", "gryphon", "pegasus", "unicorn",
               "winged_unicorn")
    ),
    Q(
        Question("Species - dragon?"),
        IfAny("dragon"),
        Implies("dragon"),
        AddAll(Implies("marine"), "aquatic_dragon"),
        AddAll(Implies("fur"), "furred_dragon"),
        AddAll(Implies("feathers"), "feathered_dragon"),
        AddAll("eastern_dragon", "western_dragon")
    ),
    Q(
        Question("Characters - positions?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll(Implies("lying"), "on_front", "on_back", "on_side"),
        AddAll(Implies("bed"), "on_bed"),
        AddAll(Implies("branch"), "on_branch"),
        AddAll(Implies("fence"), "against_fence"),
        AddAll(Implies("tree"), "against_tree"),
        AddAll(Implies("window"), "against_window"),
        AddAll(Implies("standing"), "wide_stance"),
        AddAll(Implies("leaning"), "leaning_forward"),
        AddAll("lying", "standing", "sitting", "reclining", "crouching",
               "all_fours", "against_wall", "ass_up", "presenting",
               "spreading", "bound", "upside_down", "kneeling", "bent_over",
               "leaning")
    ),
    Q(
        Question("Presenting?"),
        IfAny("presenting"),
        Implies("presenting"),
        AddAll(Implies("anus"), "presenting_anus"),
        AddAll(Implies("breasts"), "presenting_breasts"),
        AddAll(Implies("cloaca"), "presenting_cloaca"),
        AddAll(Implies("butt"), "presenting_hindquarters"),
        AddAll(Implies("penis"), "presenting_penis"),
        AddAll(Implies("balls"), "presenting_balls"),
        AddAll(Implies("pussy"), "presenting_pussy"),
        AddAll("presenting_crotch")
    ),
    Q(
        Question("Spreading?"),
        IfAny("spreading"),
        Implies("spreading"),
        AddAll(Implies("anus"), "spread_anus"),
        AddAll(Implies("butt"), "spread_butt"),
        AddAll(Implies("pussy"), "spread_pussy"),
        AddAll(Implies(MetaTag("legs")), "spread_legs")
    ),
    Q(
        Question("Characters - grabbing another character?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll("arm_grab", "breast_grab", "butt_grab", "foot_grab",
               "hair_grab", "head_grab", "leg_grab", "neck_grab",
               "torso_grab"),
        AddAll(Implies("horn"), "horn_grab"),
        AddAll(Implies(MetaTag("tail")), "tail_grab"),
        AddAll(Implies("penis"), "penis_grab"),
        AddAll(Implies("balls"), "ball_grab")
    ),
    Q(
        Question("Characters - holding object?"),
        IfAny(...CHARACTERS_IN_SCENE),
        Implies("holding_object"),
        AddAll(Implies("balls"), "holding_balls"),
        AddAll(Implies("ball"), "holding_ball"),
        AddAll(Implies("beverage"), "holding_beverage"),
        AddAll(Implies("book"), "holding_book"),
        AddAll(Implies("bottle"), "holding_bottle"),
        AddAll(Implies("breasts"), "holding_breast"),
        AddAll(Implies("camera"), "holding_camera"),
        AddAll(Implies("condom"), "holding_condom"),
        AddAll(Implies("controller"), "holding_controller"),
        AddAll(Implies("cup"), "holding_cup"),
        AddAll(Implies("flower"), "holding_flower"),
        AddAll(Implies("food"), "holding_food"),
        AddAll(Implies("gift"), "holding_gift"),
        AddAll(Implies("glass"), "holding_glass"),
        AddAll(Implies("leash"), "holding_leash"),
        AddAll(Implies("microphone"), "holding_microphone"),
        AddAll(Implies("money"), "holding_money"),
        AddAll(Implies("musical_instrument"), "holding_musical_instrument"),
        AddAll(Implies("phone"), "holding_phone"),
        AddAll(Implies("pokéball"), "holding_pokéball"),
        AddAll(Implies("sex_toy"), "holding_sex_toy"),
        AddAll(Implies("sign"), "holding_sign"),
        AddAll(Implies("tools"), "holding_tool"),
        AddAll(Implies("toy"), "holding_toy"),
        AddAll(Implies("umbrella"), "holding_umbrella"),
        AddAll(Implies("underwear"), "holding_underwear"),
        AddAll(Implies("weapon"), "holding_weapon"),
        AddAll(Implies("whip"), "holding_whip")
    ),
    Q(
        Question("Characters - actions?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll("standing", "lying", "sitting", "licking", "kneeling",
               "drooling", "undressing", "crying", "sleeping", "straddling",
               "crouching", "flying", "smoking", "reclining", "sucking",
               "leaning", "teasing", "dancing", "flexing", "eating", "panting",
               "shaking", "covering", "walking", "drinking", "running",
               "stretching", "coiling", "pointing", "trembling", "relaxing",
               "bathing", "swallowing", "reading", "floating", "tickling",
               "swimming", "jumping", "yelling", "waving", "wrestling",
               "sniffing", "struggling", "beckoning", "choking", "singing",
               "riding", "begging", "blinking", "screaming", "squeezing",
               "smothering", "falling", "flirting", "cooking", "burping",
               "dressing", "straining", "hiding", "playing", "roaring",
               "drowning", "bouncing", "brainwashing", "growling", "thinking",
               "fantasizing", "driving", "showering", "twitching",
               "scratching", "lounging", "shrinking", "twerking", "resting",
               "pouting", "poking", "climbing", "reaching", "fishing",
               "cleaning", "multitasking", "breathing", "chewing", "crawling",
               "chasing", "vibrating", "surfing", "aiming", "shooting",
               "grooming", "waiting", "purring", "transformation", "rape")
    ),
    Q(
        Question("Eyes - color?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll(...EYE_COLORS)
    ),
    Q(
        Question("Eyes - pupils?"),
        IfAny(...EYE_COLORS),
        AddAll("slit_pupils")
    ),
    Q(
        Question("Eyes - sclera color?"),
        AddAll(...SCLERA_COLORS)
    ),
    Q(
        Question("Eyes - multicolored?"),
        IfAny(
            IfAny("rainbow_eyes"),
            IfNOrMore(...EYE_COLORS, 2)
        ),
        AddAll("multicolored_eyes")
    ),
    Q(
        Question("Eyes - expression?"),
        IfAny(...EYE_COLORS),
        AddAll("wink", "half-closed_eyes", "one_eye_closed", "bedroom_eyes"),
        AddAll(Implies("<3"), "<3_eyes")
    ),
    Q(
        Question("Eyes - looking at?"),
        IfAny(...EYE_COLORS),
        AddAll("looking_at_viewer", "looking_away", "looking_back",
               "looking_down", "looking_up", "looking_at_partner",
               "looking_at_another"),
        AddAll(Implies("breasts"), "looking_at_breasts"),
        AddAll(Implies("butt"), "looking_at_butt"),
        AddAll(Implies("mirror"), "looking_at_mirror"),
        AddAll(Implies("penis"), "looking_at_penis")
    ),
    Q(
        Question("Fingers?"),
        IfAny(...CHARACTERS_IN_SCENE),
        IfAny(...ANTHRO_HUMANOID_TAGS),
        AddAll(Implies("claws"), "finger_claws"),
        AddAll("claws", "nails", "2_fingers", "3_fingers", "4_fingers",
               "5_fingers", "6_fingers")
    ),
    Q(
        Question("Feet - type?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll(Implies("toes", "claws"), "toe_claws"),
        AddAll(Implies("feet"),
               "webbed_feet", "humanoid_feet", "hooves", "talons", "toes",
               "bird_feet"),
        AddAll(Implies("4_toes"), "anisodactyl", "zygodactyl"),
        AddAll("feet", "paws")
    ),
    Q(
        Question("Feet - posture?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll("digitigrade", "plantigrade")
    ),
    Q(
        Question("Feet - actions?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll(Implies("sex", "foot_fetish"), "footjob")
    ),
    Q(
        Question("Claws - color?"),
        IfAny("claws"),
        AddAll(Implies("claws"),
               "painted_claws", "rainbow_claws", "glowing_claws",
               "black_claws", "white_claws", "blue_claws", "red_claws",
               "green_claws", "grey_claws", "pink_claws", "purple_claws",
               "brown_claws", "yellow_claws", "orange_claws", "cyan_claws",
               "teal_claws")
    ),
    Q(
        Question("Claws - type(s)?"),
        IfAny("claws"),
        AddAll(Implies("claws", "toes"), "toe_claws"),
        AddAll(Implies("claws", "wings"), "wing_claws"),
        AddAll(Implies("claws"),
               "sharp_claws", "long_claws", "big_claws", "finger_claws",
               "dewclaw", "heel_claw")
    ),
    Q(
        Question("Toes - count?"),
        IfAny("toes"),
        AddAll("2_toes", "3_toes", "4_toes", "5_toes")
    ),
    Q(
        Question("Paws - features?"),
        IfAny(...CHARACTERS_IN_SCENE),
        IfAny("paws"),
        AddAll("paws", "pawprint", "pawpads"),
        AddAll(Implies("paws"), "hindpaw")
    ),
    Q(
        Question("Tuft - type?"),
        IfAny("tuft"),
        Implies("tuft"),
        AddAll(Implies(MetaTag("tail")), "tail_tuft"),
        AddAll("arm_tuft", "ball_tuft", "cheek_tuft",
               "chest_tuft", "ear_tuft", "elbow_tufts", "feather_tuft",
               "fur_tuft", "head_tuft", "leg_tuft", "neck_tuft",
               "nipple_tuft", "shoulder_tuft")
    ),
    Q(
        Question("Body - texture?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll("feathers", "fur", "scales", MetaTag("body"), MetaTag("skin"))
    ),
    Q(
        Question("Body (texture) - colors?"),
        IfAny("body"),
        Implies(MetaTag("body")),
        AddAll(...BODY_COLORS, "translucent_body")
    ),
    Q(
        Question("Body (texture) - types?"),
        IfAny("body"),
        Implies(MetaTag("body")),
        AddAll("metallic_body", "spotted_body", "striped_body",
               "translucent_body")
    ),
    Q(
        Question("Body (texture) - multicolored?"),
        IfNOrMore(...BODY_COLORS, 2),
        Implies(MetaTag("body")),
        AddAll("multicolored_body", "two_tone_body")
    ),
    Q(
        Question("Scales - colors?"),
        IfAny("scales"),
        Implies("scales"),
        AddAll(...SCALE_COLORS)
    ),
    Q(
        Question("Scales - multicolored?"),
        IfAny(
            IfNOrMore(...SCALE_COLORS, 2),
            IfAny("rainbow_scales")
        ),
        Implies("scales"),
        AddAll("two_tone_scales", "multicolored_scales")
    ),
    Q(
        Question("Scales - patterns?"),
        IfAny("scales"),
        Implies("scales"),
        AddAll("belly_scales", "spotted_scales", "striped_scales")
    ),
    Q(
        Question("Skin - color?"),
        IfAny(MetaTag("skin")),
        Implies(MetaTag("skin")),
        AddAll(...SKIN_COLORS)
    ),
    Q(
        Question("Skin - multicolored?"),
        IfNOrMore(...SKIN_COLORS, 2),
        Implies(MetaTag("skin")),
        AddAll("two_tone_skin", "multicolored_skin")
    ),
    Q(
        Question("Skin - patterns?"),
        IfAny(MetaTag("skin")),
        Implies(MetaTag("skin")),
        AddAll("spotted_skin", "striped_skin")
    ),
    Q(
        Question("Anatomy - head?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll(Implies("horn"), "antlers"),
        AddAll(Implies("fin"), "head_fin"),
        AddAll("beak", "snout", "whiskers", "flesh_whiskers", "hair", "lips",
               "teeth", "eyebrows", "eyelashes", "bald", "tongue", "horn",
               MetaTag("nose"), MetaTag("ears"))
    ),
    Q(
        Question("Tongue - color?"),
        IfAny("tongue"),
        Implies("tongue"),
        AddAll(...TONGUE_COLORS)
    ),
    Q(
        Question("Tongue - multicolored?"),
        IfNOrMore(...TONGUE_COLORS, 2),
        Implies("tongue"),
        AddAll("multicolored_tongue", "striped_tongue")
    ),
    Q(
        Question("Tongue - type?"),
        IfAny("tongue"),
        Implies("tongue"),
        AddAll(Implies("glowing"), "glowing_tongue"),
        AddAll(Implies("penis"), "penis_tongue"),
        AddAll(Implies("tentacles"), "tentacle_tongue"),
        AddAll("long_tongue", "forked_tongue", "prehensile_tongue", "deep_tongue",
               "big_tongue", "multi_tongue", "disembodied_tongue",
               "thick_tongue", "wide_tongue", "absurdly_long_tongue")
    ),
    Q(
        Question("Horn - color?"),
        IfAny("horn"),
        Implies("horn"),
        AddAll(...HORN_COLORS)
    ),
    Q(
        Question("Horn - multicolored?"),
        IfNOrMore(...HORN_COLORS, 2),
        Implies("horn"),
        AddAll("multicolored_horn")
    ),
    Q(
        Question("Ears - color(s)?"),
        IfAny("ears"),
        Implies(MetaTag("ears")),
        AddAll(...EAR_COLORS)
    ),
    Q(
        Question("Ears - multicolored?"),
        IfNOrMore(...EAR_COLORS, 2),
        Implies(MetaTag("ears")),
        AddAll("dipstick_ears", "two_tone_ears", "multicolored_ears")
    ),
    Q(
        Question("Ears - features?"),
        IfAny("ears"),
        Implies(MetaTag("ears")),
        AddAll(Implies("tuft"), "ear_tuft"),
        AddAll(Implies("fin"), "ear_fins"),
        AddAll(Implies("frill"), "ear_frills"),
        AddAll(Implies("facial_markings"), "ear_markings"),
        AddAll("inner_ear_fluff", "earhole")
    ),
    Q(
        Question("Ears - accessories?"),
        IfAny("ears"),
        Implies(MetaTag("ears")),
        AddAll(Implies("piercing"), "ear_piercing"),
        AddAll(Implies("headphones"), "earbuds"),
        AddAll("headphones", "ear_tag", "earmuffs", "earplugs")
    ),
    Q(
        Question("Nose - color?"),
        IfAny("nose"),
        AddAll(Implies(MetaTag("nose")),
               "black_nose", "pink_nose", "brown_nose", "blue_nose",
               "red_nose", "purple_nose", "grey_nose", "green_nose",
               "orange_nose", "yellow_nose", "white_nose", "cyan_nose",
               "tan_nose", "teal_nose")
    ),
    Q(
        Question("Beak - color?"),
        IfAny("beak"),
        Implies("beak"),
        AddAll(...BEAK_COLORS)
    ),
    Q(
        Question("Beak - multicolored?"),
        IfNOrMore(...BEAK_COLORS, 2),
        Implies("beak"),
        AddAll("multicolored_beak", "dipstick_beak"),
        AddAll(Implies("multicolored_beak"), "two_tone_beak")
    ),
    Q(
        Question("Hair - color?"),
        IfAny("hair"),
        Implies("hair"),
        AddAll(...HAIR_COLORS)
    ),
    Q(
        Question("Hair - accessories?"),
        IfAny("hair"),
        Implies("hair"),
        AddAll(Implies("flower"), "flower_in_hair"),
        AddAll("hairband", "hair_ring")
    ),
    Q(
        Question("Hair - multicolored?"),
        IfNOrMore(...HAIR_COLORS, 2),
        Implies("hair"),
        AddAll(Implies("multicolored_hair"), "two_tone_hair"),
        AddAll("multicolored_hair")
    ),
    Q(
        Question("Hair - types?"),
        IfAny("hair"),
        Implies("hair"),
        AddAll("braided_hair", "short_hair", "long_hair", "ponytail",
               "flaming_hair", "gradient_hair")
    ),
    Q(
        Question("Anatomy - torso?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll(Implies("feathers"), "tail_feathers"),
        AddAll(Implies("fin"), "dorsal_fin"),
        AddAll("anus", "breasts", "nipples", MetaTag("tail"), "pecs", "wings",
               "pussy", "cloaca", "navel", "butt", "chest_spike",
               "genital_slit", "penis", "balls", "sheath")
    ),
    Q(
        Question("Anatomy - features?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll(Implies("glowing"), "bioluminescence"),
        AddAll("fin", "frill", "spikes", "stripes", "scar", "markings", "tuft")
    ),
    Q(
        Question("Anatomy - fin?"),
        IfAny("fin"),
        AddAll(Implies(MetaTag("tail")), "tail_fin"),
        AddAll("ear_fins", "arm_fins", "dorsal_fin", "head_fin")
    ),
    Q(
        Question("Anatomy - frill?"),
        IfAny("frill"),
        AddAll("ear_frills", "head_frill", "neck_frill")
    ),
    Q(
        Question("Stripes - body texture?"),
        IfAny("stripes"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll(Implies(MetaTag("body")), "striped_body"),
        AddAll(Implies("feathers"), "striped_feathers"),
        AddAll(Implies("fur"), "striped_fur"),
        AddAll(Implies("skin"), "striped_skin")
    ),
    Q(
        Question("Stripes - body parts?"),
        IfAny("stripes"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll(Implies(MetaTag("tail")), "striped_tail"),
        AddAll(Implies("penis"), "striped_penis"),
        AddAll(Implies("hair"), "striped_hair"),
        AddAll(Implies("wings"), "striped_wings"),
        AddAll(Implies("tongue"), "striped_tongue"),
        AddAll(Implies("horn"), "striped_horn")
    ),
    Q(
        Question("Stripes - clothing?"),
        IfAny("stripes"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll(Implies("legwear"), "striped_legwear"),
        AddAll(Implies("clothing"), "striped_clothing"),
        AddAll(Implies("panties"), "striped_panties"),
        AddAll(Implies("armwear"), "striped_armwear"),
        AddAll(Implies("socks"), "striped_socks"),
        AddAll(Implies("stockings"), "striped_stockings"),
        AddAll(Implies("underwear"), "striped_underwear"),
        AddAll(Implies("shirt"), "striped_shirt"),
        AddAll(Implies("bikini"), "striped_bikini"),
        AddAll(Implies("swimsuit"), "striped_swimsuit"),
        AddAll(Implies("scarf"), "striped_scarf"),
        AddAll("striped_topwear", "striped_footwear", "striped_bottomwear")
    ),
    Q(
        Question("Stripes - color?"),
        IfAny("stripes"),
        AddAll("black_stripes", "white_stripes", "grey_stripes",
               "blue_stripes", "brown_stripes", "green_stripes",
               "orange_stripes", "pink_stripes", "purple_stripes",
               "red_stripes", "yellow_stripes", "rainbow_stripes")
    ),
    Q(
        Question("Anatomy - body fluids?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll("blood", "cum", "feces", "milk", "pussy_juice", "saliva",
               "sweat", "tears", "urine")
    ),
    Q(
        Question("Countershading?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll("countershading"),
        AddAll(Implies("countershading", MetaTag("tail")),
               "countershade_tail"),
        AddAll(Implies("countershading"),
               "countershade_arms", "countershade_face", "countershade_legs",
               "countershade_torso", "reverse_countershading")
    ),
    Q(
        Question("Countershading - color?"),
        IfAny("countershading"),
        AddAll(Implies("countershading"),
               "black_countershading", "blue_countershading",
               "brown_countershading", "green_countershading",
               "grey_countershading", "orange_countershading",
               "pink_countershading", "purple_countershading",
               "red_countershading", "white_countershading",
               "yellow_countershading", "beige_countershading",
               "cyan_countershading", "tan_countershading",
               "teal_countershading")
    ),
    Q(
        Question("Belly - color?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll("white_belly", "tan_belly", "grey_belly", "yellow_belly",
               "black_belly", "blue_belly", "purple_belly", "brown_belly",
               "red_belly", "green_belly", "pink_belly")
    ),
    Q(
        Question("Facial expressions?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll(Implies("saliva"), "drooling"),
        AddAll(Implies("smile"), "grin"),
        AddAll(Implies("smile", "open_mouth"), "open_smile"),
        AddAll("looking_pleasured", "angry", "ahegao", "annoyed", "bored",
               "disappointed", "disgusted", "disturbed", "embarrassed",
               "envious", "flustered", "grumpy", "guilty", "fear", "happy",
               "jealous", "love", "lust", "proud", "sad", "scared",
               "serious", "shocked", "surprise", "unimpressed", "worried",
               "crying", "frown", "grimace", "smile", "wink", "blush",
               "eyes_closed")
    ),
    Q(
        Question("Body - accessories?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll("piercing", "makeup", "collar", "jewelry")
    ),
    Q(
        Question("Body - proportions?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll("wide_hips", "thick_thighs", "slightly_chubby", "overweight",
               "obese", "morbidly_obese", "voluptuous")
    ),
    Q(
        Question("Piercings - location?"),
        IfAny("piercing"),
        AddAll(Implies("piercing", MetaTag("nose")), "nose_piercing"),
        AddAll(Implies("piercing"),
               "ear_piercing", "facial_piercing", "eyebrow_piercing",
               "tongue_piercing", "genital_piercing"),
        AddAll(Implies("furgonomics"), "furry-specific_piercing")
    ),
    Q(
        Question("Piercings - ear piercings?"),
        IfAny("ear_piercing"),
        AddAll("ear_ring", "gauged_ear")
    ),
    Q(
        Question("Piercings - facial piercings?"),
        IfAny("facial_piercing"),
        AddAll("lip_ring", "lip_piercing"),
        AddAll(Implies("lip_piercing"), "snakebite_piercing")
    ),
    Q(
        Question("Piercings - nose piercings?"),
        IfAny("nose_piercing"),
        AddAll("bridge_piercing", "nose_ring", "septum_piercing")
    ),
    Q(
        Question("Bound - limbs bound?"),
        IfAny("bound"),
        AddAll(Implies("legs_tied"), "frogtied"),
        AddAll(Implies("muzzle_(object)"), "muzzled"),
        AddAll("arms_tied", "legs_tied", "hands_tied")
    ),
    Q(
        Question("Bondage - types?"),
        IfAny("bondage"),
        Implies("bound"),
        AddAll(Implies("penis"), "penis_bondage"),
        AddAll(Implies("breasts"), "breast_bondage"),
        AddAll(Implies(MetaTag("tail")), "tail_bondage"),
        AddAll(Implies("rope"), "rope_bondage"),
        AddAll(Implies("wings"), "wing_bondage")
    ),
    Q(
        Question("Bound - bound with?"),
        IfAny("bound"),
        AddAll(Implies(MetaTag("tail")), "tail_cuff"),
        AddAll(Implies("shackles"), "handcuffs", "ankle_cuffs"),
        AddAll(Implies("penis"), "penis_ribbon", "cock_ring"),
        AddAll(Implies("rope_bondage"), "rope_harness"),
        AddAll("blindfold", "rope", "chain", "shackles", "harness", "gag",
               "chastity_cage", "muzzle_(object)")
    ),
    Q(
        Question("Duo/group - actions?"),
        IfAll(
            IfAny(...CHARACTERS_IN_SCENE),
            IfNone("solo")
        ),
        AddAll(Implies("kissing"), "french_kissing"),
        AddAll(Implies("frech_kissing"), "extreme_french_kiss"),
        AddAll(Implies(MetaTag("tail")), "tail_coil", "entwined_tails"),
        AddAll("hug", "embrace", "leg_wrap", "eye_contact",
               "cuddling", "coiling", "caress", "carrying", "spooning",
               "petting", "peeping", "merging", "mooning", "comparing",
               "nuzzling", "lifting", "ear_biting", "stomping", "killing",
               "feeding", "strangling", "bullying", "bite", "licking",
               "straddling", "teasing", "tickling", "waving",
               "wrestling", "sniffing", "choking", "beckoning", "squeezing",
               "smothering", "flirting", "playing", "brainwashing", "chasing",
               "grooming", "rape", "spanking", "on_top")
    ),
    Q(
        Question("Duo/group - relationship?"),
        IfAll(
            IfAny(...CHARACTERS_IN_SCENE),
            IfNone("solo")
        ),
        AddAll(Implies("pokémon", "interspecies"), "poképhilia"),
        AddAll(Implies("digimon", "interspecies"), "digiphilia"),
        AddAll("predator/prey", "mount/rider_relations", "bestiality",
               "romantic_couple", "same-species_bestiality", "interspecies",
               "incest")
    ),
    Q(
        Question("Duo/group - differences?"),
        IfAll(
            IfAny(...CHARACTERS_IN_SCENE),
            IfNone("solo")
        ),
        AddAll(Implies("penis"), "penis_size_difference"),
        AddAll(Implies("breasts"), "breast_size_difference"),
        AddAll("size_difference", "age_difference", "level_difference")
    ),
    Q(
        Question("Difference - size?"),
        IfAny("size_difference"),
        // Larger
        AddAll(Implies("size_difference", "male"), "larger_male"),
        AddAll(Implies("size_difference", "female"), "larger_female"),
        AddAll(Implies("size_difference", "intersex"), "larger_intersex"),
        AddAll(Implies("size_difference", "larger_intersex", "gynomorph"),
               "larger_gynomorph"),
        AddAll(Implies("size_difference", "larger_intersex", "herm"),
               "larger_herm"),
        AddAll(Implies("size_difference", "anthro"), "larger_anthro"),
        AddAll(Implies("size_difference", "human"), "larger_human"),
        AddAll(Implies("size_difference", "feral"), "larger_feral"),
        // Smaller
        AddAll(Implies("size_difference", "male"), "smaller_male"),
        AddAll(Implies("size_difference", "female"), "smaller_female"),
        AddAll(Implies("size_difference", "intersex"), "smaller_intersex"),
        AddAll(Implies("size_difference", "smaller_intersex", "gynomorph"),
               "smaller_gynomorph"),
        AddAll(Implies("size_difference", "smaller_intersex", "herm"),
               "smaller_herm"),
        AddAll(Implies("size_difference", "smaller_intersex", "andromorph"),
               "smaller_andromorph"),
        AddAll(Implies("size_difference", "anthro"), "smaller_anthro"),
        AddAll(Implies("size_difference", "human"), "smaller_human"),
        AddAll(Implies("size_difference", "feral"), "smaller_feral"),
        // Roles
        AddAll(Implies("size_difference", "micro", "macro"),
               "micro_on_macro"),
        AddAll(Implies("size_difference"),
               "big_dom_small_sub", "small_dom_big_sub")
    ),
    Q(
        Question("Mouth?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll(Implies("teeth"), "sharp_teeth"),
        AddAll(Implies("tongue"), "tongue_out"),
        AddAll(Implies("smile", "open_mouth"), "open_smile"),
        AddAll(Implies("rose", "flower_in_mouth"), "rose_in_mouth"),
        AddAll(Implies("makeup"),
               "lipstick", "eyeliner", "mascara", "eyeshadow"),
        AddAll("open_mouth", "tongue", "fangs", "teeth", "breath",
               "flower_in_mouth")
    ),
    Q(
        Question("Feathers - types?"),
        IfAny("feathers"),
        AddAll("feather_hair")
    ),
    Q(
        Question("Feathers - color?"),
        IfAny("feathers"),
        AddAll(...FEATHER_COLORS)
    ),
    Q(
        Question("Feathers - multicolored?"),
        IfNOrMore(...FEATHER_COLORS, 2),
        AddAll("multicolored_feathers", "two_tone_feathers")
    ),
    Q(
        Question("Feathers - patterns?"),
        IfAny("feathers"),
        AddAll("spotted_feathers", "striped_feathers")
    ),
    Q(
        Question("Fur - color?"),
        IfAny("fur"),
        AddAll("black_fur", "blue_fur", "brown_fur", "green_fur", "grey_fur",
               "orange_fur", "pink_fur", "purple_fur", "red_fur", "white_fur",
               "yellow_fur", "gold_fur", "rainbow_fur", "tan_fur")
    ),
    Q(
        Question("Fur - multicolored?"),
        IfAny(
            IfAny("rainbow_fur"),
            IfNOrMore("black_fur", "blue_fur", "brown_fur", "green_fur",
                      "grey_fur", "orange_fur", "pink_fur", "purple_fur",
                      "red_fur", "white_fur", "yellow_fur", "gold_fur",
                      "rainbow_fur", "tan_fur", 2)
        ),
        AddAll("two_tone_fur", "multicolored_fur")
    ),
    Q(
        Question("Fur - patterns?"),
        IfAny("fur"),
        AddAll("spotted_fur", "striped_fur")
    ),
    Q(
        Question("Jewelry?"),
        IfAny("jewelry"),
        AddAll("anklet", "bangle", "bracelet", "crown", "choker",
               "gold_jewelry", "necklace", "ring", "toe_ring",
               "silver_jewelry", "tail_jewelry", "tiara", "waist_accessory")
    ),
    Q(
        Question("Tail - color(s)?"),
        IfAny("tail"),
        Implies(MetaTag("tail")),
        AddAll(...TAIL_COLORS)
    ),
    Q(
        Question("Tail - multicolored?"),
        IfNOrMore(...TAIL_COLORS, 2),
        Implies(MetaTag("tail")),
        AddAll("multicolored_tail", "dipstick_tail", "two_tone_tail",
               "ringtail"),
        AddAll(Implies("countershading"), "countershade_tail")
    ),
    Q(
        Question("Tail - length, thickness, size?"),
        IfAny("tail"),
        Implies(MetaTag("tail")),
        AddAll("small_tail", "big_tail", "huge_tail", "hyper_tail",
               "short_tail", "long_tail", "skinny_tail", "thick_tail")
    ),
    Q(
        Question("Tail - shape?"),
        IfAny("tail"),
        Implies(MetaTag("tail")),
        AddAll("curled_tail", "forked_tail", "mace_tail", "spade_tail",
               "braided_tail", "docked_tail", "frizzy_tail", "spiked_tail",
               "multi_tail")
    ),
    Q(
        Question("Tail - types?"),
        IfAny("tail"),
        Implies(MetaTag("tail")),
        AddAll("fish_tail", "flaming_tail", "fluffy_tail", "glowing_tail",
               "leaf_tail", "head_tail", "prehensile_tail", "snake_tail",
               "tentacle_tail", "ringtail")
    ),
    Q(
        Question("Tail - parts?"),
        IfAny("tail"),
        Implies(MetaTag("tail")),
        AddAll(Implies("fin"), "tail_fin"),
        AddAll(Implies("tuft"), "tail_tuft"),
        AddAll("tail_blade", "tail_feathers", "tail_ridge",
               "tailfro", "tail_dimple", "tail_hand", "tail_mouth",
               "tail_penis", "tail_pussy", "tail_wings")
    ),
    Q(
        Question("Tail - fashion?"),
        IfAny("tail"),
        Implies(MetaTag("tail")),
        AddAll("tail_button_warmers", "tail_warmer", "tailband",
               "tail_bell", "tail_belt", "tail_cuff", "tail_garter",
               "tail_orb", "tail_ornament", "tail_ribbon", "tail_bow",
               "tail_jewelry", "tail_ring", "tail_piercing"),
        AddAll(Implies("markings"), "tail_markings")
    ),
    Q(
        Question("Tail - position?"),
        IfAny("tail"),
        Implies(MetaTag("tail")),
        AddAll("tail_around_leg", "tail_around_waist", "tail_aside",
               "tail_between_legs", "tail_in_mouth", "tail_in_pants",
               "tail_pillow", "tail_scarf")
    ),
    Q(
        Question("Tail - activity (active)?"),
        IfAny("tail"),
        Implies(MetaTag("tail")),
        AddAll("balancing_on_tail", "coiled_tail", "entwined_tails",
               "hanging_by_tail", "raised_tail", "tailwag", "tail_boner",
               "tail_coil", "tail_growth"),
        AddAll(Implies("<3"), "tail_<3")
    ),
    Q(
        Question("Tail - activity (passive)?"),
        IfAny("tail"),
        Implies(MetaTag("tail")),
        AddAll("hand_on_tail", "holding_tail", "lifted_by_tail",
               "tail_biting", "tail_bondage", "tail_fondling",
               "tail_grab", "tail_hug", "tail_lick", "tail_pull")
    ),
    Q(
        Question("Tail - sex?"),
        IfAll(
            IfAny("tail"),
            IfAny("sex", "masturbation")
        ),
        Implies(MetaTag("tail")),
        AddAll(Implies("tail_sex"), "tailjob"),
        AddAll(Implies("tail_masturbation"), "autotailjob"),
        AddAll("tail_sex", "tail_masturbation", "tail_play", "tail_suck")
    ),
    Q(
        Question("Wings - color?"),
        IfAny("wings"),
        Implies("wings"),
        AddAll(...WING_COLORS)
    ),
    Q(
        Question("Wings - features?"),
        IfAny("wings"),
        Implies("wings"),
        AddAll(Implies("claws"), "wing_claws"),
        AddAll(Implies("piercing"), "wing_piercing")
    ),
    Q(
        Question("Wings - multicolored?"),
        IfNOrMore(...WING_COLORS, 2),
        Implies("wings"),
        AddAll(Implies("multicolored_wings"), "two_tone_wings"),
        AddAll("multicolored_wings")
    ),
    Q(
        Question("Wings - types?"),
        IfAny("wings"),
        Implies("wings"),
        AddAll("big_wings", "bone_wings"),
        AddAll(Implies("feathers"), "feathered_wings"),
        AddAll("floating_wings"),
        AddAll(Implies("fur"), "furred_wings"),
        AddAll("insect_wings", "butterfly_wings", "mechanical_wings",
               "membranous_wings", "multi_wing"),
        AddAll(Implies("multi_wing"), "4_wings"),
        AddAll("realistic_wings", "small_wings", "unusual_wings",
               "mismatched_wings")
    ),
    Q(
        Question("Wings - state?"),
        IfAny("wings"),
        Implies("wings"),
        AddAll("folded_wings", "spread_wings", "torn_wings")
    ),
    Q(
        Question("Legs?"),
        IfAny(...CHARACTERS_IN_SCENE),
        Implies(MetaTag("legs")),
        AddAll(Implies(MetaTag("legs")),
               "prosthetic_legs", "featureless_legs"),
        AddAll(MetaTag("legs"))
    ),
    Q(
        Question("Legs - position?"),
        IfAny("legs"),
        AddAll(Implies(MetaTag("legs"), "spreading"), "spread_legs"),
        AddAll(Implies(MetaTag("legs")), "legs_up", "crossed_legs",
               "one_leg_up", "on_one_leg", "raised_leg", "legs_behind_head",
               "on_hind_legs", "legs_together"),
        AddAll(Implies(MetaTag("legs"), MetaTag("tail")), "tail_between_legs")
    ),
    Q(
        Question("Hands - on someone/self (single)?"),
        IfAny(...ANTHRO_HUMANOID_TAGS),
        AddAll(Implies("breasts"), "hand_on_breast"),
        AddAll(Implies("butt"), "hand_on_butt"),
        AddAll(Implies("penis"), "hand_on_penis"),
        AddAll(Implies("underwear"), "hand_in_underwear"),
        AddAll(Implies("panties"), "hand_in_panties"),
        AddAll(Implies("balls"), "hand_on_balls"),
        AddAll(Implies("hair"), "hand_in_hair"),
        AddAll("hand_on_hip", "hand_on_head", "hand_on_thigh",
               "hand_on_stomach", "hand_on_chest", "hand_on_shoulder",
               "hand_on_leg", "hand_on_face", "hand_on_back", "hand_on_knee",
               "hand_on_arm", "hand_on_neck", "hand_behind_head",
               "hand_behind_back")
    ),
    Q(
        Question("Hands - placement (both)?"),
        IfAny(...ANTHRO_HUMANOID_TAGS),
        AddAll("hands_behind_back", "hands_behind_head", "hands_above_head",
               "hands_in_pockets", "hands_on_hip", "hands_together",
               "crossed_arms", "grabbing_sheets")
    ),
    Q(
        Question("Hands - gestures?"),
        IfAny(...ANTHRO_HUMANOID_TAGS),
        AddAll("beckoning", "devil_horns", "finger_gun", "middle_finger",
               "ok_sign", "pinky_out", "pointing", "raised_index_finger",
               "salute", "shaka", "thumbs_up", "thumbs_down", "v_sign",
               "waving", "double_v_sign", "hand_heart")
    ),
    Q(
        Question("Hands - gestures (multiple characters)?"),
        IfAny(...ANTHRO_HUMANOID_TAGS),
        IfNone("solo"),
        AddAll("fist_bump", "handshake", "high_five", "bunny_ears_(gesture)")
    ),
    Q(
        Question("Markings - color?"),
        IfAny("markings"),
        Implies("markings"),
        AddAll(...MARKING_COLORS)
    ),
    Q(
        Question("Markings - placements?"),
        IfAny(...CHARACTERS_IN_SCENE),
        Implies("markings"),
        AddAll(Implies("facial_markings"), "forehead_markings"),
        AddAll("arm_markings", "back_markings", "ball_markings",
               "breast_markings", "butt_markings", "chest_markings",
               "ear_markings", "eye_markings", "facial_markings",
               "occipital_markings", "leg_markings", "stomach_markings",
               "tail_markings")
    ),
    Q(
        Question("Markings - types?"),
        IfAny(...CHARACTERS_IN_SCENE),
        Implies("markings"),
        AddAll(Implies("glowing"), "glowing_markings"),
        AddAll("gloves_(marking)", "socks_(marking)", "fingerless_(marking)",
               "toeless_(marking)", "tribal_markings")
    ),
    Q(
        Question("Clothed or nude?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll(Implies("clothing"),
               "fully_clothed", "partially_clothed", "skimpy", "bottomless",
               "topless", "underwear", "open_shirt",  "discarded_clothing",
               "mostly_nude", "no_underwear", "clothed"),
        AddAll("nude")
    ),
    Q(
        Question("Clothing - features?"),
        IfAny("clothing"),
        AddAll("stripes")
    ),
    Q(
        Question("Clothing - eyewear?"),
        IfAny("clothing"),
        AddAll(Implies("eyewear"),
               "glasses", "goggles", "sunglasses", "eye_patch", "pince-nez",
               "monocle", "blinders")
    ),
    Q(
        Question("Clothing - headgear?"),
        IfAny("clothing"),
        Implies("clothing"),
        AddAll(Implies("headwear"), "hat", "helmet"),
        AddAll("collar")
    ),
    Q(
        Question("Clothing - torso?"),
        IfAny("clothing"),
        Implies("clothing"),
        AddAll("armwear", "shoulder_pads", "gloves", "cape", "cloak",
               "coat", "crop_top", "dress", "hoodie", "jacket",
               "leaf_clothing", "robe", "sash", "shawl", "shirt",
               "straitjacket", "sweater", "sweater_vest", "swimsuit", "suit",
               "tube_top", "tunic", "tuxedo", "vest", "waistcoat")
    ),
    Q(
        Question("Clothing - swimsuit type(s)?"),
        IfAny("swimsuit"),
        Implies("swimsuit"),
        AddAll("bikini", "one-piece_swimsuit", "school_swimsuit", "speedo",
               "swimming_trunks"),
        AddAll(Implies("bikini"),
               "sling_bikini", "micro_bikini", "string_bikini",
               "side-tie_bikini", "flag_bikini", "striped_bikini")
    ),
    Q(
        Question("Clothing - lower body?"),
        IfAny("clothing"),
        AddAll(Implies("clothing"),
               "jeans", "pants", "shorts", "skirt", "anklet", "boots",
               "high_heels", "sandals", "shoes", "socks", "loincloth")
    ),
    Q(
        Question("Clothing - armwear?"),
        IfAny("armwear"),
        Implies("armwear"),
        AddAll("arm_warmers", "bridal_gauntlets", "detached_sleeves",
               "elbow_gloves", "vambraces")
    ),
    Q(
        Question("Clothing - hat type?"),
        IfAny("hat"),
        Implies("hat"),
        AddAll("boater_hat", "bowler_hat", "fedora", "square_academic_cap",
               "top_hat", "aviator_cap", "chef_hat", "cowboy_hat", "hard_hat",
               "nurse_hat", "police_hat", "santa_hat", "sombrero", "straw_hat",
               "tokin_hat", "ushanka", "witch_hat", "wizard_hat", "beanie",
               "beret", "baseball_cap", "dunce_cap", "flat_cap", "jester_hat",
               "party_hat", "propeller_hat")
    ),
    Q(
        Question("Clothing - helmet type?"),
        IfAny("helmet"),
        Implies("helmet"),
        AddAll("combat_helmet", "hard_hat", "firefighter_helmet",
               "ancient_helmet", "winged_helmet")
    ),
    Q(
        Question("Clothing - boot types?"),
        IfAny("boots"),
        Implies("boots"),
        AddAll("cowboy_boots", "high_heeled_boots", "knee_boots",
               "thigh_boots")
    ),
    Q(
        Question("Clothing - sock type?"),
        IfAny("socks"),
        Implies("socks"),
        AddAll("knee_socks", "thigh_socks", "stirrup_socks", "toeless_socks")
    ),
    Q(
        Question("Clothing - legwear?"),
        IfAny("legwear"),
        Implies("legwear"),
        AddAll("black_legwear", "striped_legwear", "white_legwear",
               "leggings", "leg_warmer", "pantyhose", "stockings",
               "thigh_highs", "tights")
    ),
    Q(
        Question("Clothing - uniform?"),
        IfAny("clothing"),
        Implies("uniform"),
        AddAll("maid_uniform", "police_uniform", "school_uniform",
               "nurse_uniform", "military_uniform", "sailor_uniform",
               "baseball_uniform", "scout_uniform", "shinigami_uniform",
               "gym_uniform", "wonderbolts_uniform", "firefighter_uniform",
               "prison_uniform", "basketball_uniform")
    ),
    Q(
        Question("Clothing - miscellaneous?"),
        IfAny("clothing"),
        AddAll("clothed_feral", "clothing_around_neck",
               "clothing_around_waist", "discarded_clothing", "furgonomics",
               "holding_clothing", "holding_underwear", "ineffective_clothing",
               "leaf_clothing", "open_clothing", "rolled_up_sleeves",
               "sheer_clothing", "tight_clothing", "torn_clothing",
               "transparent_clothing", "underwear_on_head",
               "wardrobe_malfunction", "jewelry")
    ),
    Q(
        Question("Background detailed/simple?"),
        AddAny("detailed_background", "simple_background")
    ),
    Q(
        Question("Location?"),
        IfAny("detailed_background"),
        AddAny("inside", "outside")
    ),
    Q(
        Question("Location - time of day?"),
        IfAny("detailed_background"),
        AddAll("day", "sunset", "night")
    ),
    Q(
        Question("Location - weather?"),
        IfAny("detailed_background"),
        AddAll(Implies("snow"), "snowing"),
        AddAll("raining", "lightning", "sandstorm", "snowstorm",
               "thunderstorm", "overcast", "tornado", "wind", "fog")
    ),
    Q(
        Question("Location - outside where?"),
        IfAny("outside"),
        AddAll(Implies("tree"), "forest"),
        AddAll("beach", "city", "field", "mountain",
               "cityscape", "cloudscape", "horizon", "nature", "sky",
               "swimming_pool", "space")
    ),
    Q(
        Question("Location - inside where?"),
        IfAny("inside"),
        AddAll("cave", "bedroom", "bathroom", "bathhouse", "swimming_pool",
               "hospital", "kitchen", "office")
    ),
    Q(
        Question("Location - beach features?"),
        IfAny("beach"),
        AddAll("beach_ball", "parasol", "sand", "sand_castle", "sea", "water",
               "surfing", "swimming", "wave", "swimsuit", "cloud",
               "nude_beach")
    ),
    Q(
        Question("Location - forest features?"),
        IfAny("forest"),
        AddAll("tree", "grass")
    ),
    Q(
        Question("Location - field features?"),
        IfAny("field"),
        AddAll("grass", "savanna")
    ),
    Q(
        Question("Location - mountain features?"),
        IfAny("mountain"),
        AddAll("snow")
    ),
    Q(
        Question("Location - cave features?"),
        IfAny("cave"),
        AddAll("crystal", "lava")
    ),
    Q(
        Question("Location - bedroom features?"),
        IfAny("bedroom"),
        AddAll(Implies("bedding"), "bed_sheet", "blanket"),
        AddAll("bed", "pillow", "bedding", "mattress", "window", "curtains")
    ),
    Q(
        Question("Location - hospital features?"),
        IfAny("hospital"),
        AddAll(Implies("bed"), "hospital_bed"),
        AddAll("nurse")
    ),
    Q(
        Question("Location - sky type?"),
        IfAny("sky"),
        AddAll("blue_sky", "starry_sky")
    ),
    Q(
        Question("Water - features?"),
        IfAll("water"),
        AddAll("waterfall", "underwater", "hot_spring", "partially_submerged",
               "reflection")
    ),
    Q(
        Question("Bathing?"),
        IfAny("bath", "bathroom", "bathhouse"),
        AddAll(Implies("bath"), "bathhouse"),
        AddAll("bathtub", "rubber_duck", "sauna", "bathhouse", "shower",
               "water", "bathing")
    ),
    Q(
        Question("Props?"),
        AddAll(Implies("phone"), "cellphone"),
        AddAll("weapon", "cup", "food", "phone", "glass", "ball", "bottle",
               "book", "sex_toy", "musical_instrument", "tool", "controller",
               "towel", "umbrella", "spoon", "sign", "camera",
               "microphone", "toy", "beverage", "gift", "whip", "money",
               "plant", "staff", "jewelry")
    ),
    Q(
        Question("Weapon - melee?"),
        IfAny("weapon"),
        Implies("melee_weapon"),
        AddAll("melee_weapon"),
        AddAll(Implies("sword"),
               "katana", "rapier", "master_sword", "cutlass", "scimitar",
               "claymore", "khopesh", "greatsword", "broadsword"),
        AddAll(Implies("polearm"),
               "spear", "scythe", "trident", "halberd", "glaive", "naginata"),
        AddAll(Implies("knife"), "bayonet"),
        AddAll("sword", "polearm", "knife", "axe", "dagger", "scabbard",
               "battle_axe", "lightsaber", "club_(weapon)", "mace",
               "pitchfork", "nunchaku", "keyblade", "flail")
    ),
    Q(
        Question("Weapon - ranged?"),
        IfAny("weapon"),
        Implies("ranged_weapon"),
        AddAll("ranged_weapon"),
        AddAll(Implies("submachine_gun"),
               "uzi", "thompson_gun"),
        AddAll(Implies("gun"),
               "shotgun", "rifle", "submachine_gun", "handgun", "handcannon",
               "machine_gun", "musket"),
        AddAll(Implies("machine_gun"), "gatling_gun"),
        AddAll(Implies("gatling_gun"), "minigun"),
        AddAll(Implies("rifle"),
               "sniper_rifle", "assault_rifle"),
        AddAll(Implies("handgun"), "pistol", "revolver"),
        AddAll(Implies("ranged_weapon", "pistol"), "desert_eagle"),
        AddAll(Implies("gun"), "handgun"),
        AddAll("gun", "bow_(weapon)", "cannon", "grenade", "rocket",
               "rocket_launcher", "crossbow", "missile", "ak-47",
               "flamethrower", "plasma_gun", "laser_gun", "grenade_launcher",
               "slingshot", "m16")
    ),
    Q(
        Question("Weapon - guns (misc)?"),
        IfAny("gun"),
        AddAll(Implies("holding_weapon"), "dual_wielding"),
        AddAll("ammunition", "bullet", "holster", "silencer", "scope")
    ),
    Q(
        Question("Ball (prop) - type?"),
        IfAny("ball"),
        Implies("ball"),
        AddAll("beach_ball", "soccer_ball", "basketball_(ball)", "tennis_ball",
               "football_(ball)", "volleyball_(ball)", "baseball_(ball)")
    ),
    Q(
        Question("Plant - type?"),
        IfAny("plant"),
        Implies("plant"),
        AddAll(Implies("flower"), "rose", "sunflower"),
        AddAll(Implies("tree"),
               "christmas_tree", "palm_tree", "pine_tree"),
        AddAll("tree", "grass", "flower", "mistletoe", "holly_(plant)",
               "potted_plant")
    ),
    Q(
        Question("Sex toy?"),
        IfAny("sex_toy"),
        Implies("sex_toy"),
        AddAll("dildo", "anal_beads", "buttplug", "cock_ring",
               "penetrable_sex_toy", "sybian")
    ),
    Q(
        Question("Background - color?"),
        IfAny("simple_background"),
        IfNone("detailed_background"),
        AddAll(
            "white_background", "yellow_background", "brown_background",
            "orange_background", "green_background", "red_background",
            "black_background", "pink_background", "blue_background",
            "rainbow_background", "grey_background", "tan_background",
            "transparent_background", "textured_background",
            "purple_background")
    ),
    Q(
        Question("Background - two-toned?"),
        IfNOrMore("white_background", "yellow_background", "brown_background",
                  "orange_background", "green_background", "red_background",
                  "black_background", "pink_background", "blue_background",
                  "rainbow_background", "grey_background", "tan_background",
                  "transparent_background", "textured_background",
                  "purple_background", 2),
        AddAll("two_tone_background")
    ),
    Q(
        Question("Background - type?"),
        IfAny("simple_background", "detailed_background"),
        AddAll("abstract_background", "transparent_background",
               "gradient_background", "pattern_background",
               "blurred_background", "textured_background",
               "photo_background", "dotted_background",
               "striped_background", "blowup_background",
               "checkered_background", "spiral_background",
               "rainbow_background")
    ),
    Q(
        Question("Background - ambient creatures?"),
        IfAny("detailed_background"),
        AddAll(Implies("ambient_sealife"),
               "ambient_coral", "ambient_crab", "ambient_fish",
               "ambient_jellyfish"),
        AddAll(Implies("ambient_insect"),
               "ambient_bee", "ambient_butterfly", "ambient_firefly",
               "ambient_fly", "ambient_moth"),
        AddAll("ambient_fairy")
    ),
    Q(
        Question("Sex?"),
        IfNone("zero_pictured"),
        AddAll("after_sex", "sex", "rape")
    ),
    Q(
        Question("Masturbation?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll("masturbation", "after_masturbation"),
        AddAll(Implies("masturbation", "penis"), "penile_masturbation"),
        AddAll(Implies("masturbation", "vaginal"), "vaginal_masturbation"),
        AddAll(Implies("masturbation", "anal"), "anal_masturbation"),
        AddAll(Implies("masturbation", "oral"), "oral_masturbation"),
        AddAll(Implies("masturbation", MetaTag("tail")), "tail_masturbation"),
        AddAll(Implies("masturbation", "cloacal"), "cloacal_masturbation"),
        AddAll(Implies("masturbation", "hair"), "hair_masturbation"),
        AddAll(Implies("masturbation"),
               "group_masturbation", "two-handed_masturbation",
               "public_masturbation"),
        AddAll(Implies("penile_masturbation", "oral_masturbation"),
               "autofellatio"),
        AddAll(Implies("vaginal_masturbation", "oral_masturbation"),
               "autocunnilingus"),
        AddAll(Implies("masturbation", "penetration"), "autopenetration"),
        AddAll(Implies("anal", "oral", "anal_masturbation"), "autorimming"),
        AddAll(Implies("masturbation", "penis", "breasts"), "autotitfuck"),
        AddAll(Implies("masturbation", "fingering"), "fingering_self")
    ),
    Q(
        Question("Fingering - form?"),
        IfAny("fingering"),
        Implies("fingering"),
        AddAll(Implies("anal"), "anal_fingering"),
        AddAll(Implies("vaginal"), "vaginal_fingering"),
        AddAll(Implies("urethral"), "urethral_fingering"),
        AddAll(Implies("clitoral"), "clitoral_fingering"),
        AddAll(Implies("cloacal"), "cloacal_fingering"),
        AddAll(Implies("nipples"), "nipple_fingering"),
        AddAll(Implies("slit"), "slit_fingering"),
        AddAll(Implies("masturbation"), "fingering_self"),
        AddAll(Implies("sex"), "fingering_partner"),
        AddAll("fingering")
    ),
    Q(
        Question("Fisting - form?"),
        IfAny("fisting"),
        Implies("fisting"),
        AddAll(Implies("anal"), "anal_fisting"),
        AddAll(Implies("vaginal"), "vaginal_fisting"),
        AddAll(Implies("urethral"), "urethral_fisting"),
        AddAll("deep_fisting", "double_fisting")
    ),
    Q(
        Question("Sex - mood?"),
        IfAny("sex"),
        Implies("sex"),
        AddAll("angry_sex", "happy_sex", "rough_sex", "casual_sex",
               "disinterested_sex")
    ),
    Q(
        Question("Sex - act?"),
        IfAny("sex"),
        AddAll(Implies("sex", "foot_fetish", "penis"), "footjob"),
        AddAll(Implies("sex", "penis"), "handjob"),
        AddAll(Implies("sex", "penis", "tail_sex"), "tailjob"),
        AddAll(Implies("sex", "fingering"), "fingering_partner"),
        AddAll(Implies("sex"), "fisting", "tribadism"),
        AddAll("ball_fondling", "frottage", "grinding",
               "hot_dogging", "mutual_masturbation", "oral", "pussy_job",
               "thigh_sex", "titfuck", "vore_sex")
    ),
    Q(
        Question("Sex - act (multiple characters)?"),
        IfAll(
            IfAny("sex"),
            IfAny(...CHARACTERS_IN_SCENE),
            IfNone("solo")
        ),
        Implies("sex"),
        AddAll(Implies("group_sex"),
               "threesome", "foursome", "orgy", "gangbang"),
        AddAll(Implies("gangbang"), "reverse_gangbang"),
        AddAll(Implies("fellatio"), "collaborative_fellatio"),
        AddAll("group_sex")
    ),
    Q(
        Question("Sex - oral act?"),
        IfAny("oral"),
        AddAll(Implies("fellatio", "beak"), "beakjob"),
        AddAll("cunnilingus", "fellatio", "forced_oral", "reverse_forced_oral",
               "rimming", "deep_rimming", "snout_fuck", "tonguejob")
    ),
    Q(
        Question("Sex - position (group)?"),
        IfAll(
            IfAny("sex"),
            IfAny(...CHARACTERS_IN_SCENE),
            IfNone("solo", "duo")
        ),
        Implies("sex", "group_sex"),
        AddAll(
               "sandwich_position", "train_position", "triangle_position"),
        AddAll(Implies("oral"), "spitroast"),
        AddAll(Implies("bisexual", "sandwich_position"), "bisexual_sandwich")
    ),
    Q(
        Question("Sex - position (duo)?"),
        IfAll(
            IfAny("sex"),
            IfAny(...CHARACTERS_IN_SCENE),
            IfNone("solo")
        ),
        Implies("sex"),
        AddAll("69_position", "amazon_position", "anvil_position",
               "arch_position", "bodyguard_position", "chair_position",
               "cowgirl_position", "cradle_position", "deck_chair_position",
               "from_behind_position", "jockey_position",
               "leg_glider_position", "lotus_position", "mastery_position",
               "missionary_position", "perching_position",
               "piledriver_position", "prison_guard_position",
               "reverse_amazon_position", "reverse_cowgirl_position",
               "reverse_piledriver_position", "reverse_missionary_position",
               "reverse_wheelbarrow_position", "squat_position",
               "speed_bump_position", "spoon_position",
               "stand_and_carry_position", "t_square_position",
               "table_lotus_position", "totem_pole_position",
               "unusual_position", "wheelbarrow_position", "doggystyle")
    ),
    Q(
        Question("Sex - actions?"),
        IfAny("sex", "masturbation"),
        AddAll("hands-free", "ejaculation", "premature_ejaculation", "orgasm")
    ),
    Q(
        Question("Sex/romantic - species?"),
        IfAny("sex", "romantic"),
        AddAll(Implies("anthro"), "anthro_on_anthro"),
        AddAll(Implies("feral"), "feral_on_feral"),
        AddAll(Implies("anthro", "feral", "bestiality"), "anthro_on_feral"),
        AddAll(Implies("anthro", "human", "interspecies"), "human_on_anthro"),
        AddAll(Implies("human", "feral", "interspecies", "bestiality"),
               "human_on_feral"),
        AddAll(Implies("humanoid", "anthro"), "humanoid_on_anthro"),
        AddAll(Implies("humanoid", "feral", "bestiality"),
               "humanoid_on_feral"),
        AddAll(Implies("taur"), "taur_on_taur"),
        AddAll(Implies("anthro", "taur"), "anthro_on_taur"),
        AddAll(Implies("humanoid", "taur"), "humanoid_on_taur"),
        AddAll(Implies("human", "taur"), "human_on_taur")
    ),
    Q(
        Question("Sex/romantic - genders?"),
        IfAny("sex", "romantic"),
        AddAll(Implies("male"), "male/male"),
        AddAll(Implies("female"), "female/female"),
        AddAll(Implies("male", "female"), "male/female", "bisexual"),
        AddAll(Implies("intersex"), "intersex/intersex"),
        AddAll(Implies("intersex", "male"), "intersex/male"),
        AddAll(Implies("intersex", "female"), "intersex/female")
    ),
    Q(
        Question("Sex - on bed?"),
        IfAll(
            IfAny("sex"),
            IfAny("bed", "bedroom")
        ),
        AddAll("on_bed", "bed")
    ),
    Q(
        Question("Sex - type?"),
        IfAny("sex"),
        Implies("sex"),
        AddAll(Implies("tentacles"), "tentacle_sex"),
        AddAll(Implies("clothed"), "clothed_sex"),
        AddAll(Implies(MetaTag("tail")), "tail_sex"),
        AddAll(Implies("transformation"), "transformation_through_sex"),
        AddAll(Implies("pregnant"), "pregnant_sex"),
        AddAll(Implies("armpit_fetish"), "armpit_sex"),
        AddAll(Implies("flying"), "flying_sex"),
        AddAll(Implies("shower"), "shower_sex"),
        AddAll(Implies("genital_slit"), "slit_sex"),
        AddAll(Implies("couch"), "couch_sex"),
        AddAll(Implies("underwear"), "underwear_sex"),
        AddAll(Implies("hair"), "hair_sex"),
        AddAll(Implies("sleeping"), "sleep_sex"),
        AddAll(Implies("underwater"), "underwater_sex"),
        AddAll(Implies("beach"), "sex_on_the_beach"),
        AddAll("parallel_sex", "safe_sex", "surprise_sex", "casual_sex")
    ),
    Q(
        Question("Sexual themes?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll(Implies("bdsm"), "bondage", "domination", "sadism",
               "masochism"),
        AddAll(Implies("bestiality"), "same-species_bestiality"),
        AddAll(Implies("food"), "food_play"),
        AddAll("bdsm", "voyeur", "exhibitionism", "peeping", "walk-in",
               "imminent_sex", "bestiality", "incest")
    ),
    Q(
        Question("Penetration?"),
        IfAny("sex", "masturbation"),
        AddAll(Implies("penetration", "anal"), "anal_penetration"),
        AddAll(Implies("penetration", "oral"), "oral_penetration"),
        AddAll(Implies("anal", "vaginal", "oral"), "all_three_filled"),
        AddAll(Implies("penetration", "vaginal"), "vaginal_penetration"),
        AddAll(Implies("penetration", "navel"), "navel_penetration"),
        AddAll(Implies("penetration", "urethral"), "urethral_penetration"),
        AddAll(Implies("penetration", "genital_slit"), "slit_penetration"),
        AddAll(Implies("penetration"),
               "ambiguous_penetration", "deep_penetration"),
        AddAll("penetration")
    ),
    Q(
        Question("Penetration - gender?"),
        IfAny("penetration"),
        Implies("penetration"),
        AddAll(Implies("male"), "male_penetrating"),
        AddAll(Implies("intersex"), "intersex_penetrating"),
        AddAll(Implies("intersex_penetrating", "gynomorph"),
               "gynomorph_penetrating"),
        AddAll(Implies("intersex_penetrating", "herm"),
               "herm_penetrating"),
        AddAll(Implies("female"), "female_penetrating"),
        AddAll("ambiguous_penetrating")
    ),
    Q(
        Question("Penetration - species?"),
        IfAny("penetration"),
        IfAll(
            IfAny(...CHARACTERS_IN_SCENE),
            IfNone("solo")
        ),
        Implies("penetration"),
        AddAll(Implies("anthro_on_feral"),
               "anthro_penetrating_feral", "feral_penetrating_anthro"),
        AddAll(Implies("human_on_anthro"),
               "anthro_penetrating_human", "human_penetrating_anthro"),
        AddAll(Implies("human_on_feral"),
               "human_penetrating_feral", "feral_penetrating_human")
    ),
    Q(
        Question("Penetration - depth?"),
        IfAny("penetration"),
        AddAll("deep_penetration", "shallow_penetration")
    ),
    Q(
        Question("Tentacles - actions?"),
        IfAny("tentacles"),
        Implies("tentacles"),
        AddAll(Implies("tentacle_sex"), "tentaclejob", "tentacle_fellatio"),
        AddAll(Implies("rape", "tentacle_sex"), "tentacle_rape"),
        AddAll("tentacle_sex", "consentacles", "tentacle_in_mouth",
               "tentacle_grab", "imminent_tentacle_rape")
    ),
    Q(
        Question("Tentacles - appearance?"),
        IfAny("tentacles"),
        AddAll(Implies("tongue"), "tentacle_tongue"),
        AddAll(Implies("translucent"), "translucent_tentacles"),
        AddAll("tentacle_hair", "penis_tentacles", "crotch_tentacles",
               "tentacle_maw", "tentacle_cilia", "tentacle_mouth")
    ),
    Q(
        Question("Transformation - type?"),
        IfAny("transformation"),
        AddAll(Implies("transformation"),
               "gender_transformation", "goo_transformation",
               "inanimate_transformation", "suit_transformation",
               "sex_toy_transformation", "size_transformation",
               "forced_transformation"),
        AddAll(Implies("transformation", "cum"), "cum_transformation"),
        AddAll(Implies("transformation", "penis"), "cock_transformation"),
        AddAll(Implies("gender_transformation"),
               "mtf_transformation", "mti_transformation",
               "ftm_transformation"),
        AddAll("transformation", "post_transformation",
               "implied_transformation", "were", "takeover")
    ),
    Q(
        Question("Transformation - were?"),
        IfAny("were"),
        AddAll("werehog", "werefelid", "wereequine", "werechiropteran",
               "wererodent", "werelagomorph", "werepokémon", "werehyaenid",
               "weremonotreme", "weredragon", "weremustelid", "wereavian",
               "werefish", "werecanid", "werecaprine", "wereprocyonid")
    ),
    Q(
        Question("Transformation - form change?"),
        IfAny("transformation", "post_transformation"),
        AddAll("anthro_to_feral", "human_to_feral", "feral_to_anthro",
               "humanoid_to_anthro", "human_to_anthro", "human_to_humanoid",
               "anthro_to_inanimate", "human_to_taur")
    ),
    Q(
        Question("Dildo - type?"),
        IfAny("dildo"),
        Implies("sex_toy"),
        AddAll("double_dildo", "vibrator", "egg_vibrator", "strapon",
               "feeldoe")
    ),
    Q(
        Question("Text/pictographics?"),
        AddAll("pictographics", "text")
    ),
    Q(
        Question("Dialogue?"),
        IfAny("pictographics", "text"),
        AddAll("dialogue")
    ),
    Q(
        Question("Dialogue - type?"),
        IfAny("dialogue"),
        AddAll("dirty_talk", "pet_praise", "profanity", "porn_dialogue",
               "yelling")
    ),
    Q(
        Question("Speech/thought bubble?"),
        IfAny("dialogue"),
        AddAll("speech_bubble", "thought_bubble")
    ),
    Q(
        Question("Text language?"),
        IfAny("text"),
        Implies("text"),
        AddAll("arabic_text", "chinese_text", "danish_text", "english_text",
               "finnish_text", "french_text", "german_text", "italian_text",
               "japanese_text", "korean_text", "latin_text", "norwegian_text",
               "polish_text", "portuguese_text", "russian_text", "spanish_text",
               "swedish_text", "thai_text")
    ),
    Q(
        Question("Anthrofied Pokémon?"),
        IfAll("pokémon_(species)", "anthro"),
        AddAll("pokémorph")
    ),
    Q(
        Question("Breasts - sizes?"),
        IfAny("breasts"),
        Implies("breasts"),
        AddAll("small_breasts", "medium_breasts", "big_breasts",
               "huge_breasts", "hyper_breasts")
    ),
    Q(
        Question("Breasts - types?"),
        IfAny("breasts"),
        AddAll(Implies("feral"), "busty_feral"),
        AddAll(Implies("nipples"), "multi_nipple"),
        AddAll(Implies("breasts"),
               "multi_breast", "natural_breasts", "non-mammal_breasts"),
        AddAll("teats", "udders")
    ),
    Q(
        Question("Breasts - features?"),
        IfAny("breasts"),
        AddAll("areola", "cleavage", "featureless_breasts",
               "hair_covering_breasts", "lactating", "side_boob", "nipples")
    ),
    Q(
        Question("Breasts - interactions?"),
        IfAny("breasts"),
        AddAll(Implies("breasts", "breast_squish"), "breasts_frottage"),
        AddAll(Implies("nipples"), "nipples_touching"),
        AddAll(Implies("breasts"),
               "bouncing_breasts", "breast_pillow", "breast_rest",
               "breast_smother", "breast_squish", "hanging_breasts",
               "holding_breast")
    ),
    Q(
        Question("Breasts - fetishes?"),
        IfAny("breasts"),
        AddAll(Implies("breasts"), "breast_expansion", "breast_milking"),
        AddAll("mouth_nipples")
    ),
    Q(
        Question("Breasts - sexual?"),
        IfAny("breasts"),
        AddAll(Implies("hand_on_breast"), "breast_grab"),
        AddAll(Implies("breast_grab"), "breast_squeeze"),
        AddAll(Implies("cum"), "cum_between_breasts", "cum_on_breasts"),
        AddAll("between_breasts", "head_in_cleavage", "titfuck",
               "breast_fondling", "grope", "breast_suck", "nipple_in_pussy",
               "nipple_penetration", "nipple_stimulation", "nipple_suck",
               "self_suckle")
    ),
    Q(
        Question("Nipples - color?"),
        IfAny("nipples"),
        Implies("nipples"),
        AddAll("pink_nipples", "black_nipples", "blue_nipples",
               "brown_nipples", "purple_nipples", "green_nipples",
               "grey_nipples", "yellow_nipples", "red_nipples",
               "orange_nipples", "white_nipples", "beige_nipples",
               "teal_nipples")
    ),
    Q(
        Question("Pussy - color?"),
        IfAny("pussy"),
        Implies("pussy"),
        AddAll("black_pussy", "blue_pussy", "pink_pussy", "purple_pussy",
               "green_pussy", "yellow_pussy", "grey_pussy", "white_pussy",
               "brown_pussy", "red_pussy", "cyan_pussy", "orange_pussy",
               "beige_pussy", "multicolored_pussy")
    ),
    Q(
        Question("Pussy - actions?"),
        IfAny("pussy"),
        AddAll(Implies("pussy", "presenting"), "presenting_pussy"),
        AddAll(Implies("pussy"),
               "cunnilingus", "looking_at_pussy")
    ),
    Q(
        Question("Pussy - size?"),
        IfAny("pussy"),
        Implies("pussy"),
        AddAll("big_pussy", "hyper_pussy")
    ),
    Q(
        Question("Pussy - type?"),
        IfAny("pussy"),
        Implies("pussy"),
        AddAll(Implies("barely_visible_genitalia"), "barely_visible_pussy"),
        AddAll(Implies("piercing"), "pussy_piercing"),
        AddAll(Implies(MetaTag("tail"), "pussy"), "tail_pussy"),
        AddAll("animal_pussy", "cleft_of_venus", "pussy_mouth",
               "humanoid_pussy", "unusual_pussy"),
        AddAll(Implies("animal_pussy"),
               "equine_pussy", "canine_pussy", "feline_pussy",
               "insect_abdomen_pussy", "cervine_pussy", "bovine_pussy")
    ),
    Q(
        Question("Pussy - parts?"),
        IfAny("pussy"),
        AddAll(Implies("clitoral_hood"), "prehensile_clitoral_hood"),
        AddAll("clitoris", "clitoral_hood", "uterus", "urethra", "cervix",
               "ovaries", "vagina_dentata", "hymen")
    ),
    Q(
        Question("Anus - type?"),
        IfAny("anus"),
        Implies("anus"),
        AddAll("gaping_anus", "multi_anus", "puffy_anus", "big_anus",
               "hyper_anus")
    ),
    Q(
        Question("Anus - color?"),
        IfAny("anus"),
        AddAll(Implies("anus"),
               "pink_anus", "black_anus", "blue_anus", "purple_anus",
               "green_anus", "yellow_anus", "white_anus", "red_anus",
               "beige_anus", "orange_anus")
    ),
    Q(
        Question("Penis - color?"),
        IfAny("penis"),
        Implies("penis"),
        AddAll("rainbow_penis", "multicolored_penis", "two_tone_penis",
               "beige_penis", "black_penis", "blue_penis", "brown_penis",
               "cyan_penis", "green_penis", "grey_penis", "orange_penis",
               "pink_penis", "purple_penis", "red_penis", "translucent_penis",
               "white_penis", "yellow_penis")
    ),
    Q(
        Question("Penis - surface?"),
        IfAny("penis"),
        Implies("penis"),
        AddAll("barbed_penis", "bumped_penis", "nubbed_penis", "ribbed_penis",
               "ridged_penis", "spiked_penis", "knot"),
        AddAll(Implies("vein"), "veiny_penis")
    ),
    Q(
        Question("Penis - knotting?"),
        IfAll("penis", "knot", "penetration"),
        AddAll("knotting")
    ),
    Q(
        Question("Penis - types?"),
        IfAny("penis"),
        Implies("penis"),
        AddAll("humanoid_penis", "animal_penis", "tapering_penis",
               "anatomically_correct_penis", "flared_penis", "hybrid_penis",
               "prehensile_penis", "invisible_penis", "metal_penis",
               "bending_penis", "talking_penis", "uncut"),
        AddAll(Implies("glowing"), "glowing_penis"),
        AddAll(Implies("translucent"), "translucent_penis"),
        AddAll(Implies("stripes"), "striped_penis"),
        AddAll(Implies("vein"), "veiny_penis"),
        AddAll(Implies("equine_penis", "hybrid_penis", "knot"),
               "knotted_equine_penis"),
        AddAll(
            Implies("animal_penis"),
            "equine_penis", "canine_penis", "feline_penis",
            "cetacean_penis", "cervine_penis", "marsupial_penis",
            "porcine_penis", "lagomorph_penis", "bovine_penis",
            "turtle_penis", "raccoon_penis", "ursine_penis",
            "crocodilian_penis", "mustelid_penis"
        )
    ),
    Q(
        Question("Penis - state?"),
        IfAny("penis"),
        Implies("penis"),
        AddAny("erection", "flaccid", "half-erect", "morning_wood")
    ),
    Q(
        Question("Penis - size?"),
        IfAny("penis"),
        Implies("penis"),
        AddAll("small_penis", "micropenis", "big_penis", "huge_penis",
               "hyper_penis")
    ),
    Q(
        Question("Penis - props?"),
        IfAny("penis"),
        AddAll(Implies("condom"), "wearing_condom"),
        AddAll("condom")
    ),
    Q(
        Question("Balls - color?"),
        IfAny("balls"),
        Implies("balls"),
        AddAll("white_balls", "black_balls", "brown_balls", "blue_balls",
               "grey_balls", "yellow_balls", "tan_balls", "green_balls",
               "purple_balls", "orange_balls", "red_balls", "pink_balls",
               "beige_balls")
    ),
    Q(
        Question("Balls - actions?"),
        IfAny("balls"),
        Implies("balls"),
        AddAll(Implies("fondling"), "ball_fondling"),
        AddAll(Implies("sex", "oral", "sucking"), "ball_suck"),
        AddAll(Implies("presenting"), "presenting_balls"),
        AddAll(Implies("sniffing"), "ball_sniffing"),
        AddAll("bouncing_balls", "ball_grab", "holding_balls", "ball_worship")
    ),
    Q(
        Question("Balls - size?"),
        IfAny("balls"),
        Implies("balls"),
        AddAll("micro_balls", "small_balls", "big_balls", "huge_balls",
               "hyper_balls")
    ),
    Q(
        Question("Balls - type?"),
        IfAny("balls"),
        Implies("balls"),
        AddAll("saggy_balls", "ball_tuft", "veiny_balls", "multi_balls",
               "hairy_balls", "uniball", "marsupial_balls", "lemon_testicles",
               "glowing_balls", "backsack", "udder_balls", "ball_ring")
    ),
    Q(
        Question("Sheath - position?"),
        IfAny("sheath"),
        AddAll("fully_sheathed", "penis_tip", "docking")
    ),
    Q(
        Question("Cum visible?"),
        IfAny("penis", "after_sex", "sex"),
        AddAll("cum", "precum"),
        AddAll(Implies("precum"), "excessive_precum")
    ),
    Q(
        Question("Cum - location (head)?"),
        IfAny("cum"),
        Implies("cum"),
        AddAll(Implies("cum_inside"), "cum_in_mouth"),
        AddAll(Implies("cum_on_self"), "cum_on_own_face"),
        AddAll(Implies("tongue"), "cum_on_tongue"),
        AddAll(Implies("cum_on_self", "cum_in_mouth"), "cum_in_own_mouth"),
        AddAll("cum_on_face", "cum_on_muzzle", "cum_on_beak", "cum_in_hair",
               "cum_on_horn", "cum_on_neck")
    ),
    Q(
        Question("Cum - location (torso)?"),
        IfAny("cum"),
        Implies("cum"),
        AddAll("cum_on_body", "cum_on_shoulder", "cum_on_armpit",
               "cum_on_back", "cum_on_chest", "cum_on_stomach", "cum_inside"),
        AddAll(Implies("breasts"), "cum_on_breasts", "cum_between_breasts"),
        AddAll(Implies("nipples"), "cum_on_nipples", "cum_in_nipples"),
        AddAll(Implies("navel"), "cum_in_navel"),
        AddAll(Implies("butt"), "cum_on_butt"),
        AddAll(Implies("anus", "cum_inside"), "cum_in_ass"),
        AddAll(Implies("balls"), "cum_on_balls"),
        AddAll(Implies("penis"), "cum_on_penis", "cum_in_penis"),
        AddAll(Implies("knot"), "cum_on_knot"),
        AddAll(Implies("foreskin"), "cum_in_foreskin"),
        AddAll(Implies("pussy"), "cum_on_pussy"),
        AddAll(Implies("pussy", "cum_inside"), "cum_in_pussy"),
        AddAll(Implies("uterus", "cum_inside"), "cum_in_uterus"),
        AddAll(Implies("cloaca"), "cum_in_cloaca"),
        AddAll(Implies("genital_slit"), "cum_on_slit", "cum_in_slit")
    ),
    Q(
        Question("Cum - location (limbs)?"),
        IfAny("cum"),
        Implies("cum"),
        AddAll("cum_on_arm", "cum_on_hand", "cum_on_leg", "cum_on_feet"),
        AddAll(Implies("hooves"), "cum_on_hoof"),
        AddAll(Implies("paws"), "cum_on_paw"),
        AddAll(Implies("tail"), "cum_on_tail"),
        AddAll(Implies("wings"), "cum_on_wings")
    ),
    Q(
        Question("Cum - location (something)?"),
        IfAny("cum"),
        Implies("cum"),
        AddAll(Implies("tentacles"), "cum_in_tentacles"),
        AddAll("cum_on_clothing", "cum_on_glasses", "cum_in_clothing",
               "cum_in_pants", "cum_in_underwear", "cum_on_ground",
               "cum_on_bed", "cum_on_self")
    ),
    Q(
        Question("Cum - volume/form?"),
        IfAny("cum"),
        AddAll(Implies("cum"),
               "excessive_cum", "drinking_cum", "infinite_cum", "cum_pool",
               "cum_string"),
        AddAll(Implies("condom"), "filled_condom")
    ),
    Q(
        Question("Cum - actions?"),
        IfAny("cum"),
        AddAll(Implies("orgasm"), "hands-free"),
        AddAll(Implies("cum"), "drinking_cum", "cum_drip", "cumshot"),
        AddAll("ejaculation", "bukkake")
    ),
    Q(
        Question("Cum - cumshot type?"),
        IfAny("cumshot"),
        AddAll("hands-free", "cum_while_penetrated", "premature_ejaculation",
               "unwanted_cumshot")
    ),
    Q(
        Question("Butt - size?"),
        IfAny("butt"),
        Implies("butt"),
        AddAny("big_butt", "huge_butt", "hyper_butt")
    ),
    Q(
        Question("Perspective - view?"),
        AddAll("first_person_view", "high-angle_view", "bird's-eye_view",
               "low-angle_view", "worm's eye_view", "dutch_angle",
               "front_view", "side_view", "rear_view", "three-quarter_view",
               "upskirt", "foreshortening", "looking_through_legs")
    ),
    Q(
        Question("Perspective - multiple shots?"),
        AddAll("comic", "cutaway", "internal", "model_sheet", "sequence",
               "multiple_poses", "multiple_scenes", "sketch_page",
               "multiple_positions", "multiple_versions", "slideshow",
               "expression_sheet")
    ),
    Q(
        Question("Perspective - focus?"),
        IfAny(...CHARACTERS_IN_SCENE),
        AddAll(Implies("butt"), "butt_shot"),
        AddAll(Implies("panties"), "panty_shot"),
        AddAll(Implies("breasts"), "breast_shot"),
        AddAll(Implies("penis"), "penis_shot"),
        AddAll("mouth_shot", "crotch_shot", "torso_shot", "foot_shot",
               "foot_focus", "butt_focus", "genital_focus")
    ),
    Q(
        Question("Symbol?"),
        AddAll(Implies("gender_symbol"), "♂", "♀"),
        AddAll(Implies("sexuality_symbol"), "⚣", "⚤"),
        AddAll("<3", "!", "?", "...", "musical_note", "?!", "pawprint",
               "directional_arrow", "zzz", "♦", "</3", "♠",
               "skull_and_crossbones", "♣", "lightning_bolt",
               "four_leaf_clover", "fleur-de-lis", "©", "gender_symbol",
               "rainbow_symbol", "+", "cross", "yin_yang", "pentagram", "ankh",
               "pentacle", "swastika", "inverted_cross", "inverted_pentagram",
               "✡", "eye_of_horus", "biohazard_symbol", "$", "nuclear")
    ),
    Q(
        Question("Fetish?"),
        AddAll("bdsm", "macro", "micro", "transformation",
               "post_transformation", "vore", "scat", "inflation",
               "watersports", "foot_fetish", "fart_fetish", "armpit_fetish",
               "navel_fetish", "hoof_fetish", "spanking", "tickling",
               "infantilism", "necrophilia", "plushophilia",
               "mind_control", "tentacles")
    ),
    Q(
        Question("Mind control - type?"),
        IfAny("mind_control"),
        Implies("mind_control"),
        AddAll("hypnosis")
    ),
    Q(
        Question("Clothed sex?"),
        IfAll("sex", "clothed"),
        IfNone("mostly_nude", "nude"),
        AddAny("clothed_sex")
    ),
    Q(
        Question("solo_focus implies 2 or more!"),
        IfAll("solo_focus"),
        AddAny("duo", "group")
    )
];

var MODEL_DATA = createModelData(TAG_QUESTIONS);
var TAG_IMPLICATIONS = MODEL_DATA["implications"];
var TAG_REVERSE_IMPLICATIONS = MODEL_DATA["reverseImplications"];
var META_TAG_NAMES = MODEL_DATA["metaTagNames"];
