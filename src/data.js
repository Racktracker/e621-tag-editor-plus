Tagger.prototype.loadPostData = function() {
    var self = this;

    return new Promise((resolve, reject) => {
        var postId = $("meta[property='og:url']").attr("content").split("/").pop();
        postId = Number(postId);

        if (isNaN(postId)) {
            reject();
        }

        $.getJSON(`https://e621.net/post/show.json?id=${postId}`, function(data) {
            var tags = data["tags"].split(" ");

            var postData = self.data.post;
            postData["tags"] = [];
            postData["siteTags"] = [];
            Vue.set(postData.tagsByCategory, {});

            for (var category of self.data.tagCategories) {
                var categoryId = category.id;

                Vue.set(postData.tagsByCategory, categoryId, []);
            }

            for (var tag of tags) {
                postData.tags.push(tag);
                postData.siteTags.push(tag);

                // Fallback to general for unknown tags
                var tagCategoryId = self.data.tagCategories.find((v) => {
                    return v.name === "general";
                }).id;

                for (var categoryId of Object.keys(self.data.allTagsByCategory)) {
                    if (self.data.allTagsByCategory[categoryId].includes(tag)) {
                        tagCategoryId = categoryId;
                        break;
                    }
                }

                postData.tagsByCategory[tagCategoryId].push(tag);
            }

            self.completeTask("loadPostData");

            self.updatePageElements();
            self.updateTagCounts();
            self.updateChecklist();
            self.addImpliedMetaTags();

            resolve();
        });
    });
};

Tagger.prototype.addSuggestion = function(tag) {
    if (!this.isEditorReady()) {
        return;
    }

    this.addTag(tag, true);
    this.removeSuggestion(tag);
};

Tagger.prototype.hasTag = function(tag) {
    if (this.data.post.tags.includes(tag)) {
        return true;
    } else if (this.data.metaTags.includes(tag)) {
        return true;
    } else {
        return false;
    }
};

Tagger.prototype.getWikiPage = function(name) {
    var self = this;

    return new Promise((resolve, reject) => {
        if (self.data.localDataInUse) {
            resolve(self.data.localData.wiki[name]);
        } else {
            self.db.wiki.get({name: name}).then(function(entry) {
                resolve(entry.content);
            });
        }
    });
};

Tagger.prototype.removeSuggestion = function(tag) {
    this.data.suggestions = this.data.suggestions.filter(s => s.tag !== tag);
};

Tagger.prototype.addTag = function(
        tag, force, updateChecklist=true, updateUI=true) {
    var self = this;
    var isMetaTag = tag.type === "metaTag";
    var tagName = tag.name;

    if (!this.isEditorReady()) {
        return;
    }

    if (this.hasTag(tagName)) {
        if (updateUI) {
            this.showMessage(`Tag <code>${tagName}</code> already added.`);
        }
        return;
    }

    if (!isMetaTag && !this.data.allTags.includes(tagName) && !force) {
        if (updateUI) {
            this.showMessage(
                `Tag <code>${tagName}</code> not recognized. ` +
                `Type <code>+${tagName}</code> to add the tag anyway.`);
        }
        return;
    }

    var impliedTags = this.getImpliedTags(tagName);
    var addedImpliedTags = impliedTags.filter((t) => {
        return self.pushTag(t);
    });
    var addedImpliedTagNames = addedImpliedTags.map((t) => {
        return t.name;
    });

    this.pushTag(tag);

    if (updateUI) {
        var message = `Tag <code>${tagName}</code> added.`;

        if (addedImpliedTags.length > 0) {
            message += ` Implied tags <code>${addedImpliedTagNames.join("</code>, <code>")}</code> added.`;
        }

        this.showMessage(message);
    }

    this.updatePageElements();
    this.updateTagCounts();

    if (updateChecklist) {
        this.updateChecklist();
    }
};

Tagger.prototype.pushTag = function(tag) {
    var isMetaTag = tag.type === "metaTag";
    var tagName = tag.name;

    if (this.hasTag(tagName)) {
        return false;
    }

    if (isMetaTag) {
        this.data.metaTags.push(tagName);
    } else {
        this.data.post.tags.push(tagName);
        this.data.post.tags = this.data.post.tags.sort();

        var defaultCategoryId = this.data.tagCategories.find((v) => {
            return v.name == "general";
        }).id;

        var added = false;

        for (var categoryId of Object.keys(this.data.allTagsByCategory)) {
            if (this.data.allTagsByCategory[categoryId].includes(tagName)) {
                this.data.post.tagsByCategory[categoryId].push(tagName);
                this.data.post.tagsByCategory[categoryId].sort();
                added = true;
            }
        }

        if (!added) {
            this.data.post.tagsByCategory[defaultCategoryId].push(tagName);
            this.data.post.tagsByCategory[defaultCategoryId].sort();
        }
    }

    return true;
};

Tagger.prototype.removeTag = function(tag, updateChecklist=true, updateUI=true) {
    var isMetaTag = tag.type === "metaTag";
    var tagName = tag.name;

    if (!this.isEditorReady()) {
        return;
    }

    if (!this.hasTag(tagName)) {
        if (updateUI) {
            this.showMessage(`Tag <code>${tag}</code> not found. Did you misspell it?`);
        }
    }

    if (isMetaTag) {
        this.data.metaTags = this.data.metaTags.filter(t => t !== tagName);
    } else {
        this.data.post.tags = this.data.post.tags.filter(t => t !== tagName);

        for (var category of this.data.tagCategories) {
            var categoryId = category.id;

            this.data.post.tagsByCategory[categoryId] = this.data.post.tagsByCategory[categoryId].filter(
                (t) => { return t !== tagName; }
            );
        }
    }

    if (updateUI) {
        this.showMessage(`Tag <code>${tagName}</code> removed.`);
    }

    this.updatePageElements();
    this.updateTagCounts();

    if (updateChecklist) {
        this.updateChecklist();
    }
};

Tagger.prototype.getTagType = function(tagName) {
    if (this.suggestionModel.metaTagNames.has(tagName)) {
        return "metaTag";
    } else {
        return "tag";
    }
};

Tagger.prototype.addTagFromInput = function(tagName, force) {
    this.addTag({type: "tag", name: tagName}, force);
};

Tagger.prototype.removeTagFromInput = function(tagName) {
    this.removeTag({type: "tag", name: tagName});
};

Tagger.prototype.addMetaTag = function(tag) {
    this.addTag(MetaTag(tag)());
};

Tagger.prototype.removeMetaTag = function(tag) {
    this.removeTag(MetaTag(tag)());
};

Tagger.prototype.updateTagCounts = function() {
    var self = this;

    if (this.data.localDataInUse) {
        return;
    }

    this.db.tags.where("name").anyOf(self.data.post.tags).toArray((tags) => {
        Vue.set(self.data.post.tagUseCounts, {});

        for (var tag of tags) {
            Vue.set(self.data.post.tagUseCounts, tag.name, tag.count);
        }
    });
};

// Promise that returns true or false depending on IndexedDB's availability
Tagger.prototype.isIndexedDBAllowed = function() {
    return new Promise((resolve, reject) => {
        var request = indexedDB.open("testDatabase");
        request.onerror = function(evt) { resolve(false); };
        request.onsuccess = function(evt) { resolve(true); };
    });
};

// Load data that helps with tag suggestions
// (tag names and excerpts of wiki pages)
// This is done last because the data will take a large amount of RAM
// once it's unpacked
Tagger.prototype.loadHelperData = function() {
    var self = this;

    return new Promise((resolve, reject) => {
        // Check if we can use IndexedDB; if not, we'll just have to
        // load everything into RAM to ensure operation
        self.isIndexedDBAllowed().then((allowed) => {
            self.data.localDataInUse = !allowed;

            if (allowed) {
                console.log("IndexedDB allowed. Using it for tag and wiki data...");
                // IndexedDB allowed. Store wiki and tag data there to
                // speed up later usage
                self.loadWiki().then(() => {
                    return self.loadTags();
                }).then(() => {
                    return self.loadPostData();
                }).then(() => {
                    resolve();
                });
            } else {
                // IndexedDB not allowed. We're probably in private mode
                console.log("IndexedDB not allowed! Storing data in memory instead...");
                self.loadWiki(false).then(() => {
                    return self.loadTags(false);
                }).then(() => {
                    return self.loadPostData();
                }).then(() => {
                    this.showMessage(
                        "Tag editor couldn't save wiki and tag data locally. "
                        + "Are you in incognito mode and/or have you allowed "
                        + "this extension to store data? This is not an error: "
                        + "the tag editor will continue to work "
                        + "but it will consume more memory."
                    );
                    resolve();
                });
            }
        });
    });
};

// Get list of tags on the page, and add them to the list of tags
// from the tag snapshot
Tagger.prototype.addPostTags = function(dbTags) {
    var tags = $("li.tag-type-artist, "
                 + "li.tag-type-copyright, "
                 + "li.tag-type-character, "
                 + "li.tag-type-species, "
                 + "li.tag-type-general");

    tags = Array.from(tags).filter((el) => {
        return $(el).find("a[href*='/post/search']").length > 0;
    });
    tags = Array.from(tags).map((el) => {
        var name = $(el).find("a[href*='/post/search']").html().replace(/ /g, "_");
        var count = Number($(el).find("span.post-count").html());
        var category = $(el).attr("class").replace("tag-type-", "");
        var categoryId = this.data.tagCategories.find((tagCategory) => {
            return tagCategory.name === category;
        }).id;

        return {name: name, count: count, categoryId: categoryId};
    });

    var tagNameSet = new Set(tags.map((entry) => { return entry.name; }));

    dbTags = dbTags.filter((dbTag) => {
        return !tagNameSet.has(dbTag.name);
    });
    tags = dbTags.concat(tags);
    tags.sort((a, b) => {
        if (a == b) {
            return 0;
        } else {
            return a > b ? 1 : -1;
        }
    });

    return tags;
};

// Add meta tags such as "tail" if related tags are found
// This is done once when the tag editor is opened
Tagger.prototype.addImpliedMetaTags = function() {
    for (var metaTagName of this.suggestionModel.metaTagNames) {
        if (!(metaTagName in this.suggestionModel.tagReverseImplications)) {
            continue;
        }

        for (var tag of this.suggestionModel.tagReverseImplications[metaTagName]) {
            if (this.hasTag(tag)) {
                this.addMetaTag(metaTagName);
                break;
            }
        }
    }
};

Tagger.prototype.getAssetLocalVersion = function(asset) {
    return new Promise((resolve, reject) => {
        this.db.versions.get({name: asset}).then((entry) => {
            var v = entry == undefined ? 0 : entry["version"];
            resolve(v);
        });
    });
};

Tagger.prototype.getAssetRemoteVersion = function(asset) {
    var self = this;

    return new Promise((resolve, reject) => {
        $.getJSON(self.SCRIPT_HOST_URL + `data/${asset}_version.json`).then((data) => {
            resolve(data.version);
        }).catch((e) => {
            resolve(-1);
        });
    });
};

Tagger.prototype.getTimeUntilAssetCheck = function(asset) {
    var self = this;

    return new Promise((resolve, reject) => {
        self.db.versions.get({name: asset}).then((entry) => {
            var lastUpdateTime = entry == undefined ? 0 : entry.lastCheck;
            var currentTime = Math.floor(new Date().getTime() / 1000);

            resolve((lastUpdateTime + self.UPDATE_FREQUENCY) - currentTime);
        });
    });
};

Tagger.prototype.updateAssetCheckTimestamp = function(asset) {
    var self = this;

    return new Promise((resolve, reject) => {
        self.db.versions.where("name").equals(asset)
            .modify({lastCheck: Math.floor(new Date().getTime() / 1000)}).then(() => {
                resolve();
        });
    });
};

Tagger.prototype.getCompressedJSON = function(url) {
    return new Promise((resolve, reject) => {
        fetch(url).then((response) => {
            return response.arrayBuffer();
        }).then((data) => {
            resolve(JSON.parse(pako.inflate(data, { to: "string" })));
        }).catch((e) => {
            console.log(`Failed to retrieve resource ${url}`);
            reject();
        });;
    });
};

Tagger.prototype.loadTags = function(hasDBAccess=true) {
    var self = this;

    return new Promise((resolve, reject) => {
        if (!self.data.remainingTasks.includes("loadTagSnapshot")) {
            resolve();
        }

        if (hasDBAccess) {
            self.getTimeUntilAssetCheck("tags").then((timeUntilCheck) => {
                if (timeUntilCheck > 0) {
                    // Update not yet due
                    console.log(`Time until tag check: ${timeUntilCheck} seconds`);
                    self.loadTagSnapshotFromDB().then(() => { resolve(); });
                    return;
                }

                var localVersionQuery = new Promise((resolve, reject) => {
                    self.getAssetLocalVersion("tags").then((v) => { resolve(v); });
                });

                var remoteVersionQuery = new Promise((resolve, reject) => {
                    self.getAssetRemoteVersion("tags").then((v) => { resolve(v); });
                });

                Promise.all([localVersionQuery, remoteVersionQuery]).then((result) => {
                    var localVersion = result[0];
                    var remoteVersion = result[1];

                    console.log(`Tags up-to-date: ${localVersion == remoteVersion}`);

                    if (localVersion >= remoteVersion) {
                        // Already up-to-date

                        self.loadTagSnapshotFromDB().then(function() {
                            return self.updateAssetCheckTimestamp("tags");
                        }).then(() => {
                            resolve();
                        });
                    } else {
                        console.log("Updating tags...");
                        self.downloadTagSnapshot().then(function() {
                            return self.loadTagSnapshotFromDB();
                        }).catch(() => {
                            // Snapshot downloaded failed, check if we have one saved
                            // and fall back to that instead
                            if (localVersion > 0) {
                                self.loadTagSnapshotFromDB().then(() => { resolve(); });
                            } else {
                                throw new Error(
                                    "Couldn't download wiki snapshot, and we don't " +
                                    "have one to fall back to. Crashing and burning spectacularly, " +
                                    "please try again later."
                                );
                            }
                        }).then(() =>  {
                            return self.updateAssetCheckTimestamp("tags");
                        }).then(() => {
                            resolve();
                        }).catch((e) => {
                            this.showMessage(e);
                        });
                    }
                });
            });
        } else {
            self.downloadLocalTagSnapshot().then(function() {
                resolve();
            });
        }
    });
};

Tagger.prototype.loadTagSnapshotFromDB = function() {
    var self = this;

    return new Promise((resolve, reject) => {
        GM.getResourceUrl("searchWorkerJS").then((url) => {
            self.searchWorker = new Worker(url);

            return Promise.all([
                self.db.tags.toArray(), self.db.tag_categories.toArray()
            ]);
        }).then((data) => {
            var tags = data[0];
            var categories = data[1];
            categories.sort((a, b) => { return a.order - b.order; });

            // Add categories
            self.data.tagCategories = categories;

            var searchTagData = [];

            var tags = self.addPostTags(tags);

            for (var entry of tags) {
                if (!(entry.categoryId in self.data.allTagsByCategory)) {
                    self.data.allTagsByCategory[entry.categoryId] = [];
                }

                self.data.allTagsByCategory[entry.categoryId].push(entry.name);
                self.data.allTags.push(entry.name);

                searchTagData.push([
                    entry.name, entry.count,
                    self.data.wikiPageNames.has(entry.name)
                ]);
            }

            self.searchWorker.postMessage({
                tags: searchTagData
            });

            self.completeTask("loadTagSnapshot");

            resolve();
        });
    });
};

Tagger.prototype.downloadTagSnapshot = function() {
    var self = this;

    return new Promise((resolve, reject) => {
        self.getCompressedJSON(self.TAG_SNAPSHOT_URL).then((data) => {
            var version = data.version;

            var categoryAdditions = data.categories.map(v => ({
                name: v[0], id: v[1], order: v[2]
            }));
            var tagAdditions = data.tags.map(v => ({
                 name: v[0], count: v[1],
                 categoryId: Number(v[2])
            }));

            var tagQuery = new Promise((resolve, reject) => {
                self.db.tags.bulkPut(tagAdditions).then(() => {
                    return self.db.versions.put({
                        name: "tags", version: version
                    });
                }).then(() => { resolve(); });
            });
            var categoryQuery = self.db.tag_categories.bulkPut(
                categoryAdditions);

            Promise.all([tagQuery, categoryQuery]).then(() => {
                resolve();
            });
        });
    });
};

Tagger.prototype.downloadLocalTagSnapshot = function() {
    var self = this;

    return new Promise((resolve, reject) => {
        GM.getResourceUrl("searchWorkerJS").then((url) => {
            self.searchWorker = new Worker(url);

            return GM.getResourceUrl("tagsJSON");
        }).then((url) => {
            return self.getCompressedJSON(url);
        }).then((data) => {
            var searchTagData = [];

            var categories = data.categories;
            categories.sort((a, b) => { return a.order - b.order; });
            self.data.tagCategories = categories;

            var tags = data.tags.map((entry) => ({
                name: entry[0], count: entry[1],
                categoryId: Number(entry[2])
            }));
            tags = self.addPostTags(tags);

            for (var entry of tags) {
                if (!(entry.category in self.data.allTagsByCategory)) {
                    self.data.allTagsByCategory[entry.categoryId] = [];
                }

                self.data.allTagsByCategory[entry.categoryId].push(entry.name);
                self.data.allTags.push(entry.name);

                searchTagData.push([
                    entry.name, entry.count,
                    self.data.wikiPageNames.has(entry.name)
                ]);
            }

            self.searchWorker.postMessage({
                tags: searchTagData
            });

            self.completeTask("loadTagSnapshot");

            resolve();
        });
    });
};

Tagger.prototype.loadWiki = function(hasDBAccess=true) {
    var self = this;

    return new Promise((resolve, reject) => {
        if (!self.data.remainingTasks.includes("loadWikiSnapshot")) {
            resolve();
        }

        if (hasDBAccess) {
            self.getTimeUntilAssetCheck("wiki").then((timeUntilCheck) => {
                if (timeUntilCheck > 0) {
                    console.log(`Time until wiki check: ${timeUntilCheck} seconds`);
                    self.loadWikiSnapshotFromDB().then(() => { resolve(); });
                    return;
                }

                var localVersionQuery = new Promise((resolve, reject) => {
                    self.getAssetLocalVersion("wiki").then((v) => { resolve(v); });
                });

                var remoteVersionQuery = new Promise((resolve, reject) => {
                    self.getAssetRemoteVersion("wiki").then((v) => { resolve(v); });
                });

                Promise.all([localVersionQuery, remoteVersionQuery]).then((result) => {
                    var localVersion = result[0];
                    var remoteVersion = result[1];

                    console.log(`Wiki up-to-date: ${localVersion == remoteVersion}`);

                    if (localVersion >= remoteVersion) {
                        // Already up-to-date
                        self.loadWikiSnapshotFromDB().then(() => {
                            return self.updateAssetCheckTimestamp("wiki");
                        }).then(() => {
                            resolve();
                        });
                    } else {
                        console.log("Updating wiki...");
                        self.downloadWikiSnapshot().then(() => {
                            return self.loadWikiSnapshotFromDB();
                        }).catch(() => {
                            // We couldn't check for the new snapshot. Fallback to what we
                            // have saved, if possible
                            if (localVersion > 0) {
                                self.loadWikiSnapshotFromDB().then(() => {
                                    resolve();
                                });
                            } else {
                                throw new Error(
                                    "Couldn't download wiki snapshot, and we don't " +
                                    "have one to fall back to. Crashing and burning spectacularly, " +
                                    "please try again later."
                                );
                            }
                        }).then(() => {
                            return self.updateAssetCheckTimestamp("wiki");
                        }).then(() => {
                            resolve();
                        }).catch((e) => {
                            this.showMessage(e);
                            reject();
                        });
                    }
                });
            });
        } else {
            self.downloadLocalWikiSnapshot().then(() => {
                resolve();
            });
        }
    });
};

Tagger.prototype.loadWikiSnapshotFromDB = function() {
    var self = this;

    return new Promise((resolve, reject) => {
        self.db.wiki.orderBy("name").keys(function(wikiPageNames) {
            self.data.wikiPageNames = new Set(wikiPageNames);
            self.completeTask("loadWikiSnapshot");
            resolve();
        });
    });
};

Tagger.prototype.downloadWikiSnapshot = function() {
    var self = this;

    return new Promise((resolve, reject) => {
        self.getCompressedJSON(self.WIKI_SNAPSHOT_URL).then((data) => {
            var version = data.version;

            var additions = Object.keys(data.pages).map(
                key => ({name: key, content: data.pages[key]})
            );

            self.db.wiki.bulkPut(additions).then(() => {
                self.db.versions.put({
                    name: "wiki",
                    version: version
                }).then(() => { resolve(); });
            });
        });
    });
};

Tagger.prototype.downloadLocalWikiSnapshot = function() {
    var self = this;

    return new Promise((resolve, reject) => {
        GM.getResourceUrl("wikiCompressedJSON").then((url) => {
            return self.getCompressedJSON(url);
        }).then((data) => {
            for (var name of Object.keys(data.pages)) {
                var content = data.pages[name];

                self.data.localData.wiki[name] = content;
            }

            self.data.wikiPageNames = new Set(Object.keys(data.pages));

            self.completeTask("loadWikiSnapshot");
            resolve();
        });
    });
};

Tagger.prototype.completeTask = function(completedTask) {
    this.data.remainingTasks = this.data.remainingTasks.filter((task) => {
        return task != completedTask;
    });

    if (this.data.remainingTasks.length == 0) {
        // If there are no remaining tasks, editor is ready for use
        window.setTimeout(() => { $("#tag_editor_edit").focus(); }, 10);
    }
};

Tagger.prototype.getAddedTags = function() {
    var addedTags = this.data.post.tags.filter(
        tag => !this.data.post.siteTags.includes(tag));

    return addedTags;
};

Tagger.prototype.getRemovedTags = function() {
    var removedTags = this.data.post.siteTags.filter(
        tag => !this.data.post.tags.includes(tag));

    return removedTags;
};

Tagger.prototype.hasUnsavedChanges = function() {
    return this.getAddedTags().length > 0 || this.getRemovedTags().length > 0;
};

Tagger.prototype.isEditorReady = function() {
    var remainingTaskLength = this.data.remainingTasks.length;
    var saveDialogOpen = this.data.saveDialogOpen;

    return (
        remainingTaskLength == 0
        && !saveDialogOpen
    );
};

Tagger.prototype.saveTags = function(callback=null) {
    var self = this;

    return new Promise((resolve, reject) => {
        if (this.data.remainingTasks.includes("saveTags")) {
            return;
        };

        this.data.remainingTasks.push("saveTags");

        var editReason = this.data.saveForm.editReason;
        var updateUrl = "https://e621.net" + $("#edit-form").attr("action");

        var updateParams = {
            "authenticity_token": $("input[name='authenticity_token']").val(),
            "post[old_tags]": self.data.post.siteTags.join(" "),
            "post[tags]": self.data.post.tags.join(" ")
        };

        if (editReason != "") {
            updateParams["reason"] = editReason;
        }

        $.post(
            updateUrl, updateParams
        ).done(function(data, statusText, xhr) {
            console.log("Tags updated successfully");
            self.completeTask("saveTags");
            self.showMessage("Tags were saved successfully.");
            self.data.remainingTasks.push("loadPostData");
            self.loadPostData().then(() => {
                resolve();
            });
            return true;
        }).fail(function(data, statusText, xhr) {
            self.showMessage(
                `Tags couldn't be saved. Received message: <code>${statusText}</code>`);
            console.log("Failure?");
            console.log(data);
            reject();
        });
    });
};

Tagger.prototype.fuzzyFindTags = function(query, callback) {
    var self = this;

    var sign = "";

    if (query.length > 0 && (query[0] === "+" || query[0] === "-")) {
        sign = query[0];
        query = query.slice(1);
    };

    var handler = function(e) {
        var matches = e.data.matches;
        var origQuery = e.data.query;
        var timestamp = e.data.timestamp;

        callback(matches);
        self.searchWorker.removeEventListener("message", handler);

    }

    self.searchWorker.addEventListener("message", handler);
    self.searchWorker.postMessage({
        sign: sign, query: query, timestamp: Date.now()
    });
};

Tagger.prototype.separateMatches = function(string, substr) {
    if (substr.length == 0) {
        return [[string, false]];
    }

    if (substr[0] == "+" || substr[0] == "-") {
        substr = substr.slice(1);
    }

    var entries = [];
    var remains = string.split(substr);
    for (var i=0; i < remains.length; i++) {
        entries.push([remains[i], false]);
        if (i < remains.length - 1) {
            entries.push([substr, true]);
        };
    };

    return entries
};
