Tagger.prototype.loadSettings = function() {
    for (var name of Object.keys(this.data.defaultSettings)) {
        var defaultValue = this.data.defaultSettings[name].value;
        var valueType = this.data.defaultSettings[name].type;

        var savedValue = localStorage.getItem(`e621_tagger:${name}`);

        if (valueType === Boolean) {
            savedValue = savedValue === "true" ? true : false;
        } else {
            savedValue = valueType(savedValue);
        }

        this.data.settings[name] = savedValue !== null ? savedValue : defaultValue;
    }
};

Tagger.prototype.saveSettings = function() {
    for (var name of Object.keys(this.data.defaultSettings)) {
        var value = this.data.settings[name];

        if (value === undefined || value === null) {
            continue;
        }

        localStorage.setItem(`e621_tagger:${name}`, value);
    }
};
